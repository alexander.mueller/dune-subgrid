#ifndef DUNE_SUBGRID_HH
#define DUNE_SUBGRID_HH

/** \file
* \brief The SubGrid class
*/

#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <vector>
#include <type_traits>

#include <dune/common/version.hh>
#include <dune/common/parallel/communication.hh>

#include <dune/grid/common/backuprestore.hh>
#include <dune/grid/common/capabilities.hh>
#include <dune/grid/common/grid.hh>
#include <dune/common/timer.hh>
#include <dune/common/stdstreams.hh>

// The components of the SubGrid interface
#include "subgrid/subgridgeometry.hh"
#include "subgrid/subgridentity.hh"
#include "subgrid/subgridentityiterator.hh"
#include "subgrid/subgridentityseed.hh"
#include "subgrid/subgridleafintersectioniterator.hh"
#include "subgrid/subgridlevelintersectioniterator.hh"
#include "subgrid/subgridleveliterator.hh"
#include "subgrid/subgridleafiterator.hh"
#include "subgrid/subgridhierarchiciterator.hh"
#include "subgrid/subgridindexstorage.hh"
#include "subgrid/subgridindexsets.hh"
#include "subgrid/subgridintersection.hh"
#include "facetools/intersectioniteratorfactory.hh"

namespace Dune {

// Forward declaration
template <int dim, class HostGrid, bool MapIndexStorage>
class SubGrid;




template<int dim, class HostGrid, bool MapIndexStorage>
struct SubGridFamily
{

    typedef GridTraits<dim,
                       HostGrid::dimensionworld,
                       Dune::SubGrid<dim,HostGrid,MapIndexStorage>,
                       SubGridGeometry,
                       SubGridEntity,
                       SubGridLevelIterator,
                       SubGridLeafIntersection,
                       SubGridLevelIntersection,
                       SubGridLeafIntersectionIterator,
                       SubGridLevelIntersectionIterator,
                       SubGridHierarchicIterator,
                       SubGridLeafIterator,
                       SubGridLevelIndexSet< const SubGrid<dim,HostGrid,MapIndexStorage> >,
                       SubGridLeafIndexSet< const SubGrid<dim,HostGrid,MapIndexStorage> >,
                       SubGridGlobalIdSet< const SubGrid<dim,HostGrid,MapIndexStorage> >,
                       typename HostGrid::Traits::GlobalIdSet::IdType,
                       SubGridLocalIdSet< const SubGrid<dim,HostGrid,MapIndexStorage> >,
                       typename HostGrid::Traits::LocalIdSet::IdType,
                       Communication<SubGrid<dim,HostGrid,MapIndexStorage> >,
                       DefaultLevelGridViewTraits,
                       DefaultLeafGridViewTraits,
                       SubGridEntitySeed,
                       SubGridLocalGeometry>
            Traits;

};




//**********************************************************************
//
// --SubGrid
//
//**********************************************************************

  /** \brief [<em> provides \ref Dune::Grid </em>]
   \tparam dim Dimension of the subgrid
   \tparam HostGrid The type of the grid that we are taking elements of
   \tparam MapIndexStorage true: use a MapIndexStorage, false: use a VectorIndexStorage
  */
template <int dim, class HostGrid, bool MapIndexStorage=false>
class SubGrid :
    public GridDefaultImplementation  <dim, HostGrid::dimensionworld, double, SubGridFamily<dim,HostGrid,MapIndexStorage> >
{

    friend class SubGridLevelIndexSet<const SubGrid<dim,HostGrid,MapIndexStorage> >;
    friend class SubGridLeafIndexSet<const SubGrid<dim,HostGrid,MapIndexStorage> >;
    friend class SubGridGlobalIdSet<const SubGrid<dim,HostGrid,MapIndexStorage> >;
    friend class SubGridLocalIdSet<const SubGrid<dim,HostGrid,MapIndexStorage> >;
    friend class SubGridHierarchicIterator<const SubGrid<dim,HostGrid,MapIndexStorage> >;
    friend class SubGridLevelIntersectionIterator<const SubGrid<dim,HostGrid,MapIndexStorage> >;
    friend class SubGridLeafIntersectionIterator<const SubGrid<dim,HostGrid,MapIndexStorage> >;
    friend class SubGridLeafIntersection<const SubGrid<dim,HostGrid,MapIndexStorage> >;
    friend class SubGridLevelIntersection<const SubGrid<dim,HostGrid,MapIndexStorage> >;
    friend class SubGridHierarchicLeafFaceIterator<const SubGrid<dim,HostGrid,MapIndexStorage> >;
    friend class SubGridFatherFaceIterator<const SubGrid<dim,HostGrid,MapIndexStorage> >;
    friend class SubGridIndexStorageBase<const SubGrid<dim,HostGrid,MapIndexStorage> >;

    friend class SubGridMapIndexStorage<const SubGrid<dim,HostGrid,MapIndexStorage> >;
    friend class SubGridVectorIndexStorage<const SubGrid<dim,HostGrid,MapIndexStorage> >;

    template<int codim, PartitionIteratorType pitype, class GridImp_>
    friend class SubGridLevelIterator;

    template<int codim, PartitionIteratorType pitype, class GridImp_>
    friend class SubGridLeafIterator;

    template<int codim_, int dim_, class GridImp_>
    friend class SubGridEntity;

    template<int codim_, class GridImp_, class HostEntityIterator_>
    friend class SubGridEntityIterator;

    template<class GridImp_>
    friend class SubGridHierarchicLeafFaceIterator;

    public:

        /** \todo Should not be public */
        typedef HostGrid HostGridType;

    typedef typename std::conditional<MapIndexStorage,
                                SubGridMapIndexStorage<const SubGrid<dim,HostGrid,MapIndexStorage> >,
                                SubGridVectorIndexStorage<const SubGrid<dim,HostGrid,MapIndexStorage> > >::type IndexStorageType;

        //**********************************************************
        // The Interface Methods
        //**********************************************************

        //! type of the used GridFamily for this grid
        typedef SubGridFamily<dim,HostGrid,MapIndexStorage>  GridFamily;

        //! the Traits
        typedef typename SubGridFamily<dim,HostGrid,MapIndexStorage>::Traits Traits;

        //! The type used to store coordinates, inherited from the HostGrid
        typedef typename HostGrid::ctype ctype;

    private:

        //! The host grid which contains the actual grid hierarchy structure.
        // The hostgrid needs to be the first member in order to be initialized
        // before the indexStorage !
        HostGrid* hostgrid_;

    public:


        /** \brief Constructor
        */
        explicit SubGrid(HostGrid& hostgrid) :
            hostgrid_(&hostgrid),
            indexStorage(*this),
            globalIdSet_(*this),
            localIdSet_(*this),
            hostIntersectIteratorFactory_(hostgrid)
        {
            maxLevelDiff_ = 1;

            adaptationStep_ = nothingDone;
            hostAdaptationStep_ = nothingDone;
        }


        //! Desctructor
        ~SubGrid()
        {
            // Delete level index sets
            for (size_t i=0; i<levelIndexSets_.size(); i++)
                if (levelIndexSets_[i])
                    delete (levelIndexSets_[i]);
        }


        //! return grid name
        std::string name() const
        {
            return "SubGrid";
        }


        //! Return maximum level defined in this grid. Levels are numbered
        //! 0 ... maxlevel with 0 the coarsest level.
        int maxLevel() const {
            return maxLevel_;
        }


        //! Iterator to first entity of given codim on level
        template<int codim>
        typename Traits::template Codim<codim>::LevelIterator lbegin (int level) const{
            return SubGridLevelIterator<codim,All_Partition, const SubGrid<dim,HostGrid,MapIndexStorage> >(this, level);
        }


        //! one past the end on this level
        template<int codim>
        typename Traits::template Codim<codim>::LevelIterator lend (int level) const{
            return SubGridLevelIterator<codim,All_Partition, const SubGrid<dim,HostGrid,MapIndexStorage> >(this, level, true);
        }


        //! Iterator to first entity of given codim on level
        template<int codim, PartitionIteratorType PiType>
        typename Traits::template Codim<codim>::template Partition<PiType>::LevelIterator lbegin (int level) const{
            return SubGridLevelIterator<codim,PiType, const SubGrid<dim,HostGrid,MapIndexStorage> >(this, level);
        }


        //! one past the end on this level
        template<int codim, PartitionIteratorType PiType>
        typename Traits::template Codim<codim>::template Partition<PiType>::LevelIterator lend (int level) const{
            return SubGridLevelIterator<codim,PiType, const SubGrid<dim,HostGrid,MapIndexStorage> >(this, level, true);
        }


        //! Iterator to first leaf entity of given codim
        template<int codim>
        typename Traits::template Codim<codim>::LeafIterator leafbegin() const {
            return SubGridLeafIterator<codim,All_Partition, const SubGrid<dim,HostGrid,MapIndexStorage> >(this);
        }


        //! one past the end of the sequence of leaf entities
        template<int codim>
        typename Traits::template Codim<codim>::LeafIterator leafend() const {
            return SubGridLeafIterator<codim,All_Partition, const SubGrid<dim,HostGrid,MapIndexStorage> >(this, true);
        }


        //! Iterator to first leaf entity of given codim
        template<int codim, PartitionIteratorType PiType>
        typename Traits::template Codim<codim>::template Partition<PiType>::LeafIterator leafbegin() const {
            return SubGridLeafIterator<codim,PiType, const SubGrid<dim,HostGrid,MapIndexStorage> >(this);
        }


        //! one past the end of the sequence of leaf entities
        template<int codim, PartitionIteratorType PiType>
        typename Traits::template Codim<codim>::template Partition<PiType>::LeafIterator leafend() const {
            return SubGridLeafIterator<codim,PiType, const SubGrid<dim,HostGrid,MapIndexStorage> >(this, true);
        }


        /** \brief Number of grid entities per level and codim
        */
        int size (int level, int codim) const {
            return levelIndexSets_[level]->size(codim);
        }


        //! number of leaf entities per codim in this process
        int size (int codim) const{
            return leafIndexSet_.size(codim);
        }


        //! number of entities per level, codim and geometry type in this process
        int size (int level, GeometryType type) const {
            return levelIndexSets_[level]->size(type);
        }


        //! number of leaf entities per codim and geometry type in this process
        int size (GeometryType type) const
        {
            return leafIndexSet_.size(type);
        }


        /** \brief Access to the GlobalIdSet */
        const typename Traits::GlobalIdSet& globalIdSet() const{
            return globalIdSet_;
        }


        /** \brief Access to the LocalIdSet */
        const typename Traits::LocalIdSet& localIdSet() const{
            return localIdSet_;
        }


        /** \brief Access to the LevelIndexSets */
        const typename Traits::LevelIndexSet& levelIndexSet(int level) const
        {
            if (level<0 || level>maxLevel())
                DUNE_THROW(GridError, "levelIndexSet of nonexisting level " << level << " requested!");
            return *levelIndexSets_[level];
        }


        /** \brief Access to the LeafIndexSet */
        const typename Traits::LeafIndexSet& leafIndexSet() const
        {
            return leafIndexSet_;
        }


        /** \brief Create Entity from EntitySeed */
        template < class EntitySeed >
        typename Traits::template Codim<EntitySeed::codimension>::Entity
            entity(const EntitySeed& seed) const
        {
            typedef typename Traits::template Codim<EntitySeed::codimension>::Entity Entity;
            typedef SubGridEntity<EntitySeed::codimension, Entity::dimension, const typename Traits::Grid> EntityImp;

            return Entity(EntityImp(this, hostgrid_->entity(seed.impl().hostEntitySeed())));
        }


        /** @name Grid Refinement Methods */
        /*@{*/


        /** global refinement
        * \todo optimize implementation
        */
        void globalRefine (int refCount)
        {
            for (int i = 0; i < refCount; ++i)
            {
                for (auto&& e : elements(this->leafGridView()))
                    mark(1, e);
                preAdapt();
                adapt();
                postAdapt();
            }
        }

        /** \brief Compute and return the number of boundary segments.
         *
         *  If the number hasn't been computed yet, then do it now.
         */
        size_t numBoundarySegments () const
        {
            if (numBoundarySegments_ == -1) {

                numBoundarySegments_ = 0;
                for(const auto& element : elements(this->levelGridView(0)))
                {
                    for(const auto& intersection : intersections(this->levelGridView(0), element))
                    {
                        if(intersection.boundary())
                            ++numBoundarySegments_;
                    }
                }
            }
            return (size_t) numBoundarySegments_;
        }

        /** \brief Mark entity for refinement
        *
        * This only works for entities of codim 0.
        * The parameter is currently ignored
        *
        * \return <ul>
        * <li> true, if marking was successfull </li>
        * <li> false, if marking was not possible </li>
        * </ul>
        */
        bool mark(int refCount, const typename Traits::template Codim<0>::Entity & e)
        {
            if (adaptationStep_==preAdaptDone)
            {
                std::cout << "You did not call adapt() after preAdapt() ! Calling adapt() automatically." << std::endl;
                adapt();
            }
            if (adaptationStep_==adaptDone)
            {
                std::cout << "You did not call postAdapt() after adapt() ! Calling postAdapt() automatically." << std::endl;
                postAdapt();
            }
            adaptationStep_ = nothingDone;
            hostAdaptationStep_ = nothingDone;

            if (not(e.isLeaf()) or (refCount>1) or (refCount<-1))
                return false;

            int level = e.level();
            int index = levelIndexSet(level).index(e);

            if (refCount==1)
            {
                refinementMark_[level][index] = true;
                coarseningMark_[level][index] = false;
            }
            if (refCount==-1)
            {
                if (level>0)
                {
                    refinementMark_[level][index] = false;
                    coarseningMark_[level][index] = true;
                }
                else
                    return false;
            }
            if (refCount==0)
            {
                refinementMark_[level][index] = false;
                coarseningMark_[level][index] = false;
            }
            return true;
        }


        /** \brief Return refinement mark for entity
        *
        * \return refinement mark (1,0,-1)
        */
        int getMark(const typename Traits::template Codim<0>::Entity & e) const
        {
            if ((adaptationStep_==preAdaptDone) or (adaptationStep_==adaptDone))
                DUNE_THROW(InvalidStateException, "You can not use getMark() after preAdapt() or adapt() !");

            if (not(e.isLeaf()))
                return 0;

            int level = e.level();
            int index = levelIndexSet(level).index(e);

            if (refinementMark_[level][index])
                return 1;
            if (coarseningMark_[level][index])
                return -1;

            return 0;
        }


    private:
        class NeighborLevels
        {
            public:
                NeighborLevels(const SubGrid<dim,HostGrid,MapIndexStorage>& subgrid) :
                    subgrid_(subgrid),
                    maxElementLevel_(subgrid.size(dim), 0)
                {}

                char getMaxNeighborLevel(const typename Traits::template Codim<0>::Entity & e) const
                {
                    char maxNeighborLevel = 0;
                    for (size_t i = 0; i<e.subEntities(dim); ++i)
                    {
                        int nodeIndex = subgrid_.leafIndexSet().subIndex(e, i, dim);
                        if (maxNeighborLevel < maxElementLevel_[nodeIndex])
                            maxNeighborLevel = maxElementLevel_[nodeIndex];
                    }
                    return maxNeighborLevel;
                }

                void setElementLevel(const typename Traits::template Codim<0>::Entity & e, char newLevel)
                {
                    for (size_t i = 0; i<e.subEntities(dim); ++i)
                    {
                        int nodeIndex = subgrid_.leafIndexSet().subIndex(e, i, dim);
                        if (maxElementLevel_[nodeIndex] < newLevel)
                            maxElementLevel_[nodeIndex] = newLevel;
                    }
                }

            private:
                const SubGrid<dim,HostGrid,MapIndexStorage>& subgrid_;
                std::vector<char> maxElementLevel_;
        };

    public:
        //! \todo Please doc me !
        bool preAdapt()
        {
            if (adaptationStep_==preAdaptDone)
            {
                std::cout << "You already called call preAdapt() ! Aborting preAdapt()." << std::endl;
                return false;
            }
            if (adaptationStep_==adaptDone)
            {
                std::cout << "You did not call postAdapt() after adapt() ! Calling postAdapt() automatically." << std::endl;
                postAdapt();
            }
            adaptationStep_ = nothingDone;
            hostAdaptationStep_ = nothingDone;

            #ifdef SUBGRID_VERBOSE
            std::cout << "preadapt 1 (start)" << std::endl;
            #endif

            NeighborLevels neighborLevels(*this);

            for (auto&& e : elements(this->leafGridView()))
            {
                int index = levelIndexSet(e.level()).index(e);
                int newLevel = e.level();
                if (refinementMark_[e.level()][index])
                    ++newLevel;
                if (coarseningMark_[e.level()][index])
                    --newLevel;

                neighborLevels.setElementLevel(e, newLevel);
            }

            for (int level=maxLevel(); level>=0; --level)
            {
                bool hasImplicitRefinementMarks = false;
                bool hasCoarseningMarks = false;

                for (auto&& e : elements(this->levelGridView(level)))
                {
                    if(e.isLeaf())
                    {
                        // compute maximal level of neighbours
                        char maxNeighborLevel = neighborLevels.getMaxNeighborLevel(e);

                        // compute minimal allowed level for this element
                        int minAllowedLevel = maxNeighborLevel-maxLevelDiff_;

                        int index = levelIndexSet(level).index(e);

                        // set refinement mark if neccessary
                        if ((level<minAllowedLevel) and not(refinementMark_[level][index]))
                        {
                            refinementMark_[level][index] = true;
                            coarseningMark_[level][index] = false;
                            neighborLevels.setElementLevel(e, level+1);
                            hasImplicitRefinementMarks = true;
                        }

                        // remove coarsening mark if neccessary
                        if ((level-1<minAllowedLevel) and (coarseningMark_[level][index]))
                        {
                            coarseningMark_[level][index] = false;
                            neighborLevels.setElementLevel(e, level);
                        }

                        if (coarseningMark_[level][index])
                            hasCoarseningMarks = true;
                    }
                }

                // if there are coarsening marks left and we have set
                // implicit refinement marks check again if coarsening is possible
                if (hasCoarseningMarks and hasImplicitRefinementMarks)
                {
                    for (auto&& e : elements(this->levelGridView(level)))
                    {
                        if(e.isLeaf())
                        {
                            // compute maximal level of neighbours
                            char maxNeighborLevel = neighborLevels.getMaxNeighborLevel(e);

                            // compute minimal allowed level for this element
                            int minAllowedLevel = maxNeighborLevel-maxLevelDiff_;

                            int index = levelIndexSet(level).index(e);

                            // remove coarsening mark if neccessary
                            if ((level-1<minAllowedLevel) and (coarseningMark_[level][index]))
                            {
                                coarseningMark_[level][index] = false;
                                neighborLevels.setElementLevel(e, level);
                            }
                        }
                    }
                }
            }

            #ifdef SUBGRID_VERBOSE
            std::cout << "preadapt 2 (refinement/coarsening marks for neighbours set)" << std::endl;
            #endif

            bool mightBeCoarsened = false;
            bool callHostgridAdapt = false;

            for (auto&& e : elements(this->leafGridView()))
            {
                int level = e.level();
                int index = levelIndexSet(level).index(e);

                // if element is marked for coarsening
                if (coarseningMark_[level][index])
                {
                    const auto& father = e.father();
                    int fatherLevel = father.level();
                    int fatherIndex = levelIndexSet(fatherLevel).index(father);

                    // if father is not processed
                    //    then look for brothers
                    //    sets the coarsening mark for father if children may be coarsened
                    //    sets the refinement mark for father if not
                    if (not(refinementMark_[fatherLevel][fatherIndex]) and not(coarseningMark_[fatherLevel][fatherIndex]))
                    {
                        // iterate over all brothers
                        for (const auto& h : descendantElements(father,level))
                        {
                            // if brother is not marked for coarsening
                            //    then mark father for not coarsening and stop
                            int brotherIndex = levelIndexSet(level).index(h);
                            if (not(coarseningMark_[level][brotherIndex]))
                            {
                                refinementMark_[fatherLevel][fatherIndex] = true;
                                break;
                            }
                        }
                        // if father was not marked for not coarsening
                        //    then mark it for coarsening
                        if (not(refinementMark_[fatherLevel][fatherIndex]))
                            coarseningMark_[fatherLevel][fatherIndex] = true;
                    }

                    // if father is marked for not coarsening
                    //    then unset element mark for coarsening
                    if (refinementMark_[fatherLevel][fatherIndex])
                        coarseningMark_[level][index] = false;
                }
                else
                {
                    if (refinementMark_[level][index])
                    {
                        if (e.impl().hostEntity().isLeaf())
                        {
                            hostgrid_->mark(1, e.impl().hostEntity());
                            callHostgridAdapt = true;
                        }
                    }
                }
            }

            #ifdef SUBGRID_VERBOSE
            std::cout << "preadapt 3 (checked coarsening marks, marked host grid)" << std::endl;
            #endif

            if (callHostgridAdapt)
            {
                hostgrid_->preAdapt();

                hostAdaptationStep_ = preAdaptDone;

                #ifdef SUBGRID_VERBOSE
                std::cout << "preadapt 4 (host grid preadapt called)" << std::endl;
                #endif
            }

            adaptationStep_ = preAdaptDone;

            return mightBeCoarsened;
        }


        //! Triggers the grid refinement process
        bool adapt()
        {
            if ((adaptationStep_==nothingDone) or (adaptationStep_==postAdaptDone))
            {
                std::cout << "You did not call preAdapt() before adapt() ! Calling preAdapt() automatically." << std::endl;
                preAdapt();
            }
            if (adaptationStep_==adaptDone)
            {
                std::cout << "You already called call adapt() ! Aborting adapt()." << std::endl;
                return false;
            }

            typedef typename Traits::GlobalIdSet GlobalIdSet;
            typedef typename GlobalIdSet::IdType GlobalIdType;

            #ifdef SUBGRID_VERBOSE
            std::cout << "adapt 1 (start)" << std::endl;
            #endif

            std::map<GlobalIdType, bool> elementMarks;
            for(int level=0; level <= maxLevel(); ++level)
            {
                for (auto&& e : elements(this->levelGridView(level)))
                {
                    int level = e.level();
                    int index = levelIndexSet(level).index(e);

                    // if element is leaf
                    //    then if not marked for coarsening store it and its refinement mark
                    // if element is not leaf
                    //    then store it and ignore refinement mark
                    if (e.isLeaf())
                    {

                        if (not(coarseningMark_[level][index]))
                            elementMarks[globalIdSet().id(e)] = refinementMark_[level][index];
                    }
                    else
                        elementMarks[globalIdSet().id(e)] = false;
                }
            }

            #ifdef SUBGRID_VERBOSE
            std::cout << "adapt 2 (element ids stored)" << std::endl;
            #endif

            if (hostAdaptationStep_==preAdaptDone)
            {
                hostgrid_->adapt();

                hostAdaptationStep_ = adaptDone;

                #ifdef SUBGRID_VERBOSE
                std::cout << "adapt 3 (host grid adapt called)" << std::endl;
                #endif
            }

            createBegin();
            // recreate entity mark vectors

            // mark entities
            for(int level=0; level <= hostgrid_->maxLevel(); ++level)
            {
                for (auto&& e : elements(hostgrid_->levelGridView(level)))
                {
                    auto contained = elementMarks.find(hostgrid_->globalIdSet().id(e));
                    if (contained != elementMarks.end())
                    {
                        insertRaw(e);
                        if (contained->second)
                        {
                          for (auto&& hE : descendantElements(e, e.level()+1))
                              insertRaw(hE);
                        }
                    }
                }
            }

            #ifdef SUBGRID_VERBOSE
            std::cout << "adapt 4 (subgrid elements inserted)" << std::endl;
            #endif

            createEnd();

            #ifdef SUBGRID_VERBOSE
            std::cout << "adapt 5 (subgrid created)" << std::endl;
            #endif

            for(int level=0; level < maxLevel(); ++level)
            {
                for (auto&& e : elements(this->levelGridView(level)))
                {
                    auto eIt = elementMarks.find(globalIdSet().id(e));
                    if (eIt != elementMarks.end())
                    {
                        if (eIt->second == true)
                            refinementMark_[level][levelIndexSet(level).index(e)] = true;
                    }
                }
            }

            #ifdef SUBGRID_VERBOSE
            std::cout << "adapt 6 (wasRefined marks set)" << std::endl;
            #endif

            adaptationStep_ = adaptDone;

            return true;
        }


        /** \brief Clean up refinement markers */
        void postAdapt(){
            if (adaptationStep_==nothingDone)
            {
                std::cout << "You did not call preAdapt() before adapt() ! Calling preAdapt() automatically." << std::endl;
                preAdapt();
            }
            if (adaptationStep_==preAdaptDone)
            {
                std::cout << "You did not call adapt() before postAdaptt() ! Calling adapt() automatically." << std::endl;
                adapt();
            }
            if (adaptationStep_==postAdaptDone)
            {
                std::cout << "You already called call postAdapt() ! Aborting postAdapt()." << std::endl;
                return;
            }

            #ifdef SUBGRID_VERBOSE
            std::cout << "postadapt 1 (start)" << std::endl;
            #endif

            for (int level=0; level<=maxLevel(); ++level)
                refinementMark_[level].assign(refinementMark_[level].size(), false);

            if (hostAdaptationStep_ == adaptDone)
            {
                hostgrid_->postAdapt();

                hostAdaptationStep_ = postAdaptDone;

                #ifdef SUBGRID_VERBOSE
                std::cout << "postadapt 2 (host grid postadapt called)" << std::endl;
                #endif
            }

            adaptationStep_ = postAdaptDone;

            return;
        }

        /*@}*/


        /** \brief Size of the overlap on the leaf level */
        unsigned int overlapSize(int codim) const {
            return hostgrid_->leafGridView().overlapSize(codim);
        }


        /** \brief Size of the ghost cell layer on the leaf level */
        unsigned int ghostSize(int codim) const {
            return hostgrid_->leafGridView().ghostSize(codim);
        }


        /** \brief Size of the overlap on a given level */
        unsigned int overlapSize(int level, int codim) const {
            return hostgrid_->levelGridView(level).overlapSize(codim);
        }


        /** \brief Size of the ghost cell layer on a given level */
        unsigned int ghostSize(int level, int codim) const {
            return hostgrid_->levelGridView(level).ghostSize(codim);
        }


#if 0
        /** \brief Distributes this grid over the available nodes in a distributed machine
        *
        * \param minlevel The coarsest grid level that gets distributed
        * \param maxlevel does currently get ignored
        */
        void loadBalance(int strategy, int minlevel, int depth, int maxlevel, int minelement){
            DUNE_THROW(NotImplemented, "SubGrid::loadBalance()");
        }

        /** \brief The communication interface
        *  @param T: array class holding data associated with the entities
        *  @param P: type used to gather/scatter data in and out of the message buffer
        *  @param codim: communicate entites of given codim
        *  @param if: one of the predifined interface types, throws error if it is not implemented
        *  @param level: communicate for entities on the given level
        *
        *  Implements a generic communication function sending an object of type P for each entity
        *  in the intersection of two processors. P has two methods gather and scatter that implement
        *  the protocol. Therefore P is called the "protocol class".
        */
        template<class T, template<class> class P, int codim>
        void communicate (T& t, InterfaceType iftype, CommunicationDirection dir, int level);

        /*! The new communication interface

        communicate objects for all codims on a given level
        */
        template<class DataHandle>
        void communicate (DataHandle& data, InterfaceType iftype, CommunicationDirection dir, int level) const
        {}

        template<class DataHandle>
        void communicate (DataHandle& data, InterfaceType iftype, CommunicationDirection dir) const
        {}
#endif

        /** \brief Communicate data of level gridView */
        template <class DataHandle>
        void communicate (DataHandle& /*handle*/, InterfaceType /*iftype*/,
                          CommunicationDirection /*dir*/, int /*level*/) const
        {
            DUNE_THROW(Dune::NotImplemented, "communicate() for SubGrid not implemented");
        }

        /** \brief Communicate data of leaf gridView */
        template <class DataHandle>
        void communicate (DataHandle& /*handle*/, InterfaceType /*iftype*/,
                          CommunicationDirection /*dir*/) const
        {
            DUNE_THROW(Dune::NotImplemented, "communicate() for SubGrid not implemented");
        }


        /** dummy collective communication */
        const typename Traits::CollectiveCommunication& comm () const
        {
            return ccobj;
        }


        // **********************************************************
        // End of Interface Methods
        // **********************************************************

        /** @name Subgrid Creation Methods */
        /*@{*/

        /** \brief Begin subgrid creation
        *
        * Begins subgrid creation. After calling this method you can insert
        * elements to the subgrid using the insert*() methods. To complete
        * subgrid creation you must call createEnd()
        *
        * This method can also called again to recreate an already created
        * subgrid. In this case all information in this object belonging
        * to the old subgrid is lost and new and old indices in the subgrid
        * may differ. (like adaptation)
        */
        void createBegin()
        {
            for (int i=0; i<=dim; ++i) {
                entities_[i].resize(hostgrid_->maxLevel()+1);

                for (int j=0; j<=hostgrid_->maxLevel(); ++j)
                {
                    entities_[i][j].resize(hostgrid_->size(j, dim-i));
                    entities_[i][j].assign(entities_[i][j].size(), false);
                }
            }
            insertRawCalled = false;
            insertPartialCalled = false;
        }


        /** \brief Complete subgrid creation
        *
        * You should first call createBegin() and then insert elements to
        * the subgrid using the insert*() methods. CreateEnd() will by default end with
        * a subgrid conforming to the requested maximum level difference (-> setMaxLevelDifference() )
        *
        * This may take some time since the hostgrid is traversed to mark
        * subentities and to compute level and leaf indices.
        *
        * \param conform enforce the requested maximal level difference (default = true). This will have no effect if any elements have been inserted via insertPartial()
        */
        void createEnd(bool conform=true)
        {
            // ///////////////////////////////////////////////////////////
            //   Compute the number of levels in this subgrid
            // ///////////////////////////////////////////////////////////
            setMaxLevel();

            // ///////////////////////////////////////////////////////////
            //  enforce conformity level as given by maxLevelDiff_.
            //  This will NOT be performed if insertions have been done with insertPartial.
            //  If maxLevelDiff_ >= maxLevel grid is already conforming, so we can save the effort.
            //  Same would be true if grid had been created via insertLeaf, but...
            // ///////////////////////////////////////////////////////////
            if (conform and !insertPartialCalled and (unsigned int) maxLevel()>maxLevelDiff_)
                conformify();

            // ////////////////////////////////////////////////////////////////////////
            //   So far only elements have been marked as belonging to the subgrid.
            //   We now also mark all subentities of marked entities.
            // ////////////////////////////////////////////////////////////////////////
            for (int i=0; i<=maxLevel(); i++)
            {
                for (auto&& e : elements(hostgrid_->levelGridView(i)))
                {

                    // Do nothing if this element is not marked
                    if (entities_[dim][i][hostgrid_->levelIndexSet(i).index(e)]) {

                        for (size_t codim=1; codim<=dim; codim++) {
                            size_t nSubEntities = ReferenceElements<ctype,dim>::general(e.type()).size(codim);

                            for (size_t j=0; j<nSubEntities; j++)
                                entities_[dim-codim][i][hostgrid_->levelIndexSet(i).subIndex(e,j, codim)] = true;
                        }
                    }
                }
            }

            // ////////////////////////////////////////////////////////////////////////
            //   Update the index sets
            // ////////////////////////////////////////////////////////////////////////
            setIndices();

            refinementMark_.resize(maxLevel_+1);
            coarseningMark_.resize(maxLevel_+1);
            for (int j=0; j<=maxLevel_; j++)
            {
                refinementMark_[j].resize(size(j, 0), false);
                coarseningMark_[j].resize(size(j, 0), false);
                refinementMark_[j].assign(refinementMark_[j].size(), false);
                coarseningMark_[j].assign(coarseningMark_[j].size(), false);
            }

            // Update IntersectionIteratorFactory for hostgrid_;
            hostIntersectIteratorFactory_.update();
        }

        /** \brief Insert this element only !
        *
        * Calling this methods may result in an unconsistent subgrid !
        * Therefore it should only be used internally.
        *
        * If you know what you're doing you may use it externally to
        * speed up subgrid creation. In this case you must take care
        * for consistency yourself.
        */
        void insertRaw(const typename HostGrid::Traits::template Codim<HostGrid::dimension-dim>::Entity & e )
        {
            insertRawCalled = true;

            const int level = e.level();

            // set bit for the element
            entities_[dim][level][hostgrid_->levelIndexSet(level).index(e)] = true;
        }


        /** \brief Insert hostgrid level !
        *
        * Add a complete hostgrid level to the subgrid.
        * This will also insert all lower levels.
        */
        void insertLevel(int level)
        {
            for (int i=0; i<=dim; ++i)
            {
                for (int l=0; l<=level; ++l)
                    entities_[i][l].assign(entities_[i][l].size(), true);
            }
        }


        /** \brief Insert hostgrid leaf level !
        *
        * Add the complete hostgrid leaf level to the subgrid.
        * This will also insert all lower level elements.
        */
        void insertLeaf()
        {
            for (int i=0; i<=dim; ++i)
            {
                for (int l=0; l<=hostgrid_->maxLevel(); ++l)
                    entities_[i][l].assign(entities_[i][l].size(), true);
            }
        }


        /** \brief Inserts all elements in the container, all ancestors and all direct children of the ancestors
        *
        * This will result in a subgrid where the leaf level covers the same domain as level 0.
        * Although this domain might be smaller than the domain covered level 0 of the hostgrid
        */
        template <class Container>
        void insertSet(const Container& idContainer)
        {
            for(int level=0; level <= hostgrid_->maxLevel(); ++level)
                for (const auto& e: elements(hostgrid_->levelGridView(level)))
                    if (idContainer.find(hostgrid_->globalIdSet().id(e)) != idContainer.end())
                        insert(e);
        }


        /** \brief Inserts all elements in the container plus all their ancestors
        *
        *   This will NOT necessarily result in a subgrid where the leaf level covers the same domain as level 0.
        */
        template <class Container>
        void insertSetPartial(const Container& idContainer)
        {
            #ifdef SUBGRID_TIME
            Dune::Timer timer;
            std::cout << "subgrid.insertSetPartial(): " << std::endl << "{" << std::endl;
            #endif

            for(int level=0; level <= hostgrid_->maxLevel(); ++level)
                for (const auto& e : elements(hostgrid_->levelGridView(level)))
                    if (idContainer.find(hostgrid_->globalIdSet().id(e)) != idContainer.end())
                        insertPartial(e);

            #ifdef SUBGRID_TIME
            std::cout << "}" << std::endl << " -> " << timer.elapsed() << std::endl;
            #endif
        }


        /** \brief Inserts this element, all ancestors and all direct children of the ancestors
        *
        * This will result in a subgrid where the leaf level covers the same domain as level 0.
        * Although this domain might be smaller than the domain covered by level 0 of the hostgrid
        */
        void insert(const typename HostGrid::Traits::template Codim<HostGrid::dimension-dim>::Entity & e )
        {
            int level = e.level();
            if (level == 0)
                entities_[dim][level][hostgrid_->levelIndexSet(level).index(e)] = true;
            else
            {
                auto father = e.father();

                for (const auto& child : descendantElements(father,level))
                    entities_[dim][level][hostgrid_->levelIndexSet(level).index(child)] = true;

                bool fatherInserted = entities_[dim][father.level()][hostgrid_->levelIndexSet(father.level()).index(father)];
                if (not(fatherInserted) or insertRawCalled or insertPartialCalled)
                    insert(father);
            }
        }


        /** \brief Inserts this element and all ancestors
        *
        * This will in general result in a subgrid where each level covers a different domain.
        * The domain covered by the leaf level might be much smaller than the domains covered
        * by the coarser levels.
        * This might be useful to get algebraic coarse levels even if the domain cannot be
        * resolved by coarser grids.
        */
        void insertPartial(const typename HostGrid::Traits::template Codim<HostGrid::dimension-dim>::Entity & e )
        {
            insertPartialCalled = true;;

            int level = e.level();
            entities_[dim][level][hostgrid_->levelIndexSet(level).index(e)] = true;

            if (level > 0)
            {
                auto father = e.father();

                bool fatherInserted = entities_[dim][father.level()][hostgrid_->levelIndexSet(father.level()).index(father)];
                if (not(fatherInserted) or insertRawCalled)
                    insertPartial(father);
            }
        }


        /*@}*/


        /** @name Special Subgrid Methods */
        /*@{*/


        //! Returns true if hostgrid entity is contained in subgrid
        template <int codim>
        bool contains(const typename HostGrid::Traits::template Codim<codim>::Entity& e ) const
        {
            const int level = e.level();
            return entities_[dim-codim][level][hostgrid_->levelIndexSet(level).index(e)];
        }


        //! Returns the hostgrid this subgrid lives in
        HostGridType& getHostGrid() const
        {
            return *hostgrid_;
        }


        //! Returns the hostgrid entity encapsulated in given subgrid entity
        template <int codim>
        typename HostGrid::Traits::template Codim<codim>::Entity getHostEntity(const typename Traits::template Codim<codim>::Entity& e) const
        {
            return e.impl().hostEntity();
        }


        //! Returns the subgrid entity encapsulating a given hostgrid entity
        // or throws an exception if it is not contained in the subgrid
        template <int codim>
        typename Traits::template Codim<codim>::Entity getSubGridEntity(const typename HostGrid::Traits::template Codim<codim>::Entity& e) const
        {
            if (not(contains<codim>(e)))
                DUNE_THROW(GridError, "Hostgrid entity is not contained in the subgrid!");
            typedef typename HostGrid::Traits::template Codim<codim>::Entity HostGridEntity;
            return SubGridEntity<codim,HostGrid::dimension,const SubGrid<dim,HostGrid,MapIndexStorage> >(this, HostGridEntity(e));
        }


        /** \brief Track hostgrid adaptation
        *
        * Returns true if *adapt() method of hostgrid was called
        * during last run of *adapt() in subgrid
        */
        bool hostGridAdapted () const
        {
            return (hostAdaptationStep_!=nothingDone);
        }


        //! Set the maximal difference of levels that elements containing a common vertex should have
        void setMaxLevelDifference(int maxLevelDiff)
        {
            maxLevelDiff_ = maxLevelDiff;
            return;
        }


        //! \todo Please doc me !
        template<class ElementTransfer>
        void transfer(ElementTransfer& elementTransfer) const
        {
            typedef typename HostGrid::template Codim<0>::Entity HostGridElement;

            int hostMaxLevel = hostgrid_->maxLevel();

            elementTransfer.pre();

            for (auto&& e : elements(this->leafGridView()))
            {
                HostGridElement hostElement = getHostEntity<0>(e);
                if (hostElement.isLeaf())
                    elementTransfer.transfer(e, hostElement);
                else
                {
                    for (const auto& h : descendantElements(hostElement,hostMaxLevel))
                    {
                        if (h.isLeaf())
                            elementTransfer.transfer(e, h);
                    }
                }
            }
            elementTransfer.post();
        }


        /** \brief Shrink hostgrid to subgrid size
        *
        * This method shrinks the hostgrid while the subgrid is preserved.
        * Up to maxAdaptations coarsening steps will be applied until the
        * number of hostgrid leaf elements remains unchanged. In general
        * the resulting hostgrid might still be containe more elements
        * than the subgrid.
        *
        * Observe, that this method changes the hostgrid AND the subgrid.
        * The subgrid should contain the same elements afterwards, but
        * the indices might have changed!
        *
        * You should not call mark() or preAdapt() befor and postAdapt()
        * afer this method for subgrid or hostgrid.
        *
        * The state after calling this method is comparable to the
        * state after postAdapt() for subgrid and hostgrid.
        *
        * \param maxAdaptations maximal number of coarsening steps (default=100)
        *
        * \param recreateSubgrid If true, subgrid containing the same elements is recreated,
        * if false, subgrid is not usable until created manually. (default=true)
        */
        bool shrinkHostGrid(int maxAdaptations = 100, bool recreateSubgrid = true)
        {
            if (adaptationStep_==preAdaptDone)
            {
                std::cout << "You did not call adapt() after preAdapt() ! Calling adapt() automatically." << std::endl;
                adapt();
            }
            if (adaptationStep_==adaptDone)
            {
                std::cout << "You did not call postAdapt() after adapt() ! Calling postAdapt() automatically." << std::endl;
                postAdapt();
            }
            adaptationStep_ = nothingDone;
            hostAdaptationStep_ = nothingDone;

            typedef typename Traits::GlobalIdSet GlobalIdSet;
            typedef typename GlobalIdSet::IdType GlobalIdType;


            std::map<GlobalIdType, int> elementLevels;

            // remember subgrid elements
            int subgridMaxLevel = maxLevel();
            {
                for (auto&& e : elements(this->leafGridView()))
                    elementLevels[globalIdSet().id(e)] = e.level();
            }


            bool hostGridChanged = true;
            int hostGridLeafElementSize;
            int adaptations = 0;
            while ((adaptations<maxAdaptations) and hostGridChanged)
            {
                hostGridLeafElementSize = hostgrid_->size(0);
                {
                    for (auto&& e : elements(hostgrid_->leafGridView()))
                    {
                        if (e.level() > subgridMaxLevel)
                            hostgrid_->mark(-1, e);
                        else
                        {
                            auto found = elementLevels.find(hostgrid_->globalIdSet().id(e));
                            if (found == elementLevels.end())
                                hostgrid_->mark(-1, e);
                            else if (found->second < e.level())
                                hostgrid_->mark(-1, e);
                        }
                    }
                }
                hostgrid_->preAdapt();
                hostgrid_->adapt();
                hostgrid_->postAdapt();

                ++adaptations;
                if (hostGridLeafElementSize == hostgrid_->size(0))
                    hostGridChanged = false;
            }

            // restore subgrid
            if (recreateSubgrid)
            {
                createBegin();
                for(int level=0; level <= subgridMaxLevel; ++level)
                {
                    for (auto&& e : elements(hostgrid_->levelGridView(level)))
                    {
                        auto found = elementLevels.find(hostgrid_->globalIdSet().id(e));
                        if (found != elementLevels.end())
                        {
                            if (found->second == e.level())
                                insertPartial(e);
                        }
                    }
                }
                createEnd();
            }

            return hostGridChanged;
        }


        //! Print subgrid status information
        void report() const
        {
            indexStorage.report();

            std::cout << "Subgrid covers (leaf level): ";
            std::cout << size(dim) << " of " << hostgrid_->size(dim) << " nodes";
            std::cout << " ~ " << size(dim) *100 / hostgrid_->size(dim) << " %" << std::endl;
            std::cout.precision(8);

            std::cout << "                             ";
            std::cout << size(0) << " of " << hostgrid_->size(0) << " elements";
            std::cout.precision(3);
            std::cout << " ~ " << size(0) *100 / hostgrid_->size(0) << " %" << std::endl;
            std::cout.precision(8);

            return;
        }

        /*@}*/


    protected:

        //! \todo Please doc me !
        template <int codim>
        bool isLeaf(const typename Traits::template Codim<codim>::Entity& e) const
        {
            return indexStorage.template isLeaf<codim>(e);
        }

        //! \todo Please doc me !
        template <int codim>
        bool isLeaf(const typename HostGrid::Traits::template Codim<codim>::Entity& e) const
        {
            return indexStorage.template isLeaf<codim>(e);
        }

        //! \todo Please doc me !
        bool wasRefined(const typename Traits::template Codim<0>::Entity& e) const
        {
            if (adaptationStep_!=adaptDone)
                return false;

            int level = e.level();
            int index = levelIndexSet(level).index(e);
            return refinementMark_[level][index];
        }

        //! Provide access to IntersectionIteratorFactory for the HostGrid
        const typename Facetools::IntersectionIteratorFactory<HostGrid>& hostIIFactory() const
        {
            return hostIntersectIteratorFactory_;
        }



        /** \brief Enforces the maxLevelDiff_ and returns whether any changes had to be performed.
         *
         *  The requested maximal level difference maxLevelDiff_ of neighboring elements is enforced. The neighbor relation is here defined as
         *  being across vertices not edges for simplicity of implementation. The resulting Grid contains the grid that would result using the edge-neighbor relation.
         *
         *  The method assumes that the current maxLevel of the SubGrid is already determined and Subentities and indexSets are inserted/computed afterwards (outside this method).
         *  Therefore it is called by default at the corresponding point in createEnd() if insertPartial() has NOT been called.
         *
         */
        bool conformify()
        {
            bool subgridChanged = false;
            /* map that stores the maximal level on which a subgrid node appears (nodeID --> maxSubgridLevel(node)) */
            std::map<typename Traits::LocalIdSet::IdType , unsigned int> subgridNodesLevel;

            /* we start at the maxLevel and work our way down to level 1;
             * level 0 is omitted as it should already be contained in the
             * subgrid (if insertPartial or insertRaw have not been called) */
            for (unsigned int l = maxLevel(); l > 0; --l)
            {
                const auto& levelView = hostgrid_->levelGridView(l);
                /* loop over hostgrid elements on current level l and insert
                 * vertices into the map if element is in subgrid.
                 * otherwise check if the map value of the vertex is larger
                 * than allowed by the maxLevelDiff and, if so,
                 * insert the element into the subgrid and all its
                 * vertices and those of its brothers into the map */
                for (const auto& e : elements(levelView))
                {
                    if (entities_[dim][l][hostgrid_->levelIndexSet(l).index(e)]) {
                        const auto& geometry = e.geometry();
                        for (int v=0; v < geometry.corners(); ++v)
                            /* (nodeID,l) is only inserted if map does not already contain a value to key nodeID, therefore the higher level value prevails */
                            subgridNodesLevel.insert(std::make_pair(hostgrid_->localIdSet().subId(e,v,dim), l));
                    } else
                        /* not on the highest level as there will not be any maxLevel elements to be inserted to conformify the subgrid (maxLevelDiff_ >= 1) */
                        if (l < (unsigned int)maxLevel()) {
                            const auto& geometry = e.geometry();
                            for (int v=0; v < geometry.corners(); ++v)
                            {
                                auto mapping = subgridNodesLevel.find(hostgrid_->localIdSet().subId(e,v,dim));
                                /* if vertex is already in map and level difference is too high insert current hostelement to subgrid */
                                if (mapping != subgridNodesLevel.end() and mapping->second - l >= maxLevelDiff_)
                                {
                                    insert(e);
                                    /* with insertion of this element, all its
                                     * brothers have also been inserted, so we
                                     * need to insert all brother vertices into
                                     * the map or update their level */
                                    for(const auto& brother : descendantElements(e.father(), l)) {
                                        for (int vv=0; vv < brother.geometry().corners(); ++vv) {
                                            auto vvSubId = hostgrid_->localIdSet().subId(e,vv,dim);
                                            bool wasInserted = subgridNodesLevel.insert(std::make_pair(vvSubId, l)).second;
                                            if (not wasInserted)
                                                subgridNodesLevel[vvSubId] = std::max(subgridNodesLevel[vvSubId],l);
                                        }
                                    }
                                    /* if we inserted this element, we can leave it */
                                    subgridChanged = true;
                                    break;
                                }
                            }
                        }
                }
            }

            return subgridChanged;
        }

        //! \todo Please doc me !
        IndexStorageType indexStorage;


    private:

        //! compute the maximal level appearing in subgrid
        void setMaxLevel() {
            maxLevel_ = -1;
            for (int i=entities_[dim].size()-1;  i>=0; i--)
                if (countBits(entities_[dim][i]) > 0) {
                    maxLevel_ = i;
                    break;
                }
        }

        int countBits(const std::vector<bool>& bits) const {
            int n = 0;
            for (size_t i=0; i<bits.size(); i++)
                n += (bits[i]) ? 1 : 0;
            return n;
        }


        //! compute the subgrid indices and id's
        void setIndices()
        {
#ifdef SUBGRID_VERBOSE
            Timer timer;
#endif
            localIdSet_.update();

            globalIdSet_.update();

            indexStorage.update();

            // //////////////////////////////////////////
            //   Create the index sets
            // //////////////////////////////////////////
            for (int i=levelIndexSets_.size(); i<=maxLevel(); i++)
            {
                SubGridLevelIndexSet<const SubGrid<dim,HostGrid,MapIndexStorage> >* p
                    = new SubGridLevelIndexSet<const SubGrid<dim,HostGrid,MapIndexStorage> >();
                levelIndexSets_.push_back(p);
            }

            for (int i=0; i<=maxLevel(); i++)
                if (levelIndexSets_[i])
                    levelIndexSets_[i]->update(*this, i);

            leafIndexSet_.update(*this);

            #ifdef SUBGRID_VERBOSE
            std::cout << "SubGrid indices set in " << timer.elapsed() << " seconds." << std::endl;
            #endif
        }

        /** For each entity of each codim in each level of the hostgrid: a bit
        * saying whether that entity forms part of the SubGrid
        */
        std::vector<std::vector<bool> > entities_[dim+1];

        //! \todo Please doc me !
        std::vector<std::vector<bool> > refinementMark_;

        //! \todo Please doc me !
        std::vector<std::vector<bool> > coarseningMark_;

        //! Stores the maximal level appearing in subgrid
        int maxLevel_;

        //! Cache the number of boundary segments and only compute them when required
        mutable int numBoundarySegments_ = -1;

        //! \todo Please doc me !
        typename Traits::CollectiveCommunication ccobj;

        //! Our set of level indices
        std::vector<SubGridLevelIndexSet<const SubGrid<dim,HostGrid,MapIndexStorage> >*> levelIndexSets_;

        //! \todo Please doc me !
        SubGridLeafIndexSet<const SubGrid<dim,HostGrid,MapIndexStorage> > leafIndexSet_;

        //! \todo Please doc me !
        SubGridGlobalIdSet<const SubGrid<dim,HostGrid,MapIndexStorage> > globalIdSet_;

        //! \todo Please doc me !
        SubGridLocalIdSet<const SubGrid<dim,HostGrid,MapIndexStorage> > localIdSet_;

        //! Stores the maximal difference of levels than elements containing a common vertex should have
        unsigned int maxLevelDiff_;

        //! Defines the possible steps in adaptation cycle
        enum AdaptationStep {nothingDone, preAdaptDone, adaptDone, postAdaptDone};

        //! Stores current adaptation step of subgrid
        AdaptationStep adaptationStep_;

        //! Stores current adaptation step of host grid
        AdaptationStep hostAdaptationStep_;

        //! remember if insertRaw was called
        bool insertRawCalled;

        //! remember if insertPartial was called
        bool insertPartialCalled;

        //! Use a IntersectionIteratorFactory to simplify access to HostGrid intersection iterators
        typename Facetools::IntersectionIteratorFactory<HostGrid> hostIntersectIteratorFactory_;

}; // end Class SubGrid




namespace Capabilities
{
    //! \todo Please doc me !
    template<int dim, class HostGrid, bool MapIndexStorage, int codim>
    struct hasEntity< SubGrid<dim,HostGrid,MapIndexStorage>, codim>
    {
        static const bool v = hasEntity<HostGrid,codim>::v;
    };

    //! \todo Please doc me !
    template<int dim, class HostGrid, bool MapIndexStorage, int codim>
    struct hasEntityIterator< SubGrid<dim,HostGrid,MapIndexStorage>, codim>
    {
        static const bool v = hasEntityIterator<HostGrid,codim>::v;
    };

    //! \todo Please doc me !
    template<int dim, class HostGrid, bool MapIndexStorage>
    struct isLevelwiseConforming< SubGrid<dim,HostGrid,MapIndexStorage> >
    {
        static const bool v = isLevelwiseConforming<HostGrid>::v;
    };

    //! \todo Please doc me !
    template<int dim, class HostGrid, bool MapIndexStorage>
    struct isLeafwiseConforming< SubGrid<dim,HostGrid,MapIndexStorage> >
    {
        static const bool v = false;
    };

    //! \brief subgrid provides an implementation for the dune-grid BackupRestoreFacilities
    template<int dim, class HostGrid, bool MapIndexStorage>
    struct hasBackupRestoreFacilities< SubGrid<dim,HostGrid,MapIndexStorage> >
    {
        static const bool v = true;
    };
}

template<int dim, class HostGrid, bool MapIndexStorage>
struct BackupRestoreFacility< SubGrid<dim,HostGrid,MapIndexStorage> >
{
    static_assert(Dune::Capabilities::hasBackupRestoreFacilities<HostGrid>::v, "Underlying Hostgrid does not support the BackupRestoreFacilities."); // TODO only show that for SaveHostGrid branch

    using Subgrid = SubGrid<dim,HostGrid,MapIndexStorage>;
    enum SaveHostGrid { True, False }; // TODO maybe specialize the template struct instead

    /**
     * @brief backup write subgrid to disk
     * @param subgrid the subgrid
     * @param filename base filename for subgrid (and eventually hostgrid)
     * @param saveHostGrid flag if hostgrid should be saved automatically as well
     */
    static void backup(const Subgrid& subgrid, const std::string& filename, const SaveHostGrid saveHostGrid = SaveHostGrid::True)
    {
      if(saveHostGrid == SaveHostGrid::True)
        backupHostgrid(subgrid.getHostGrid(), filename);

      backupSubgrid(subgrid, filename);
    }

    // TODO it is probably a very bad idea to write this to one stream consecutively as it depends on the termination of the hostgrid backup/restore methods whether this works properly
    //! \todo
    static void backup(const Subgrid& subgrid, std::ostream& stream, const SaveHostGrid saveHostGrid = SaveHostGrid::True)
    {
      Dune::dwarn << "The dune-subgrid backup for streams is not save!";

      if(saveHostGrid == SaveHostGrid::True)
        backupHostgrid(*(subgrid.hostgrid_), stream);

      backupSubgrid(subgrid, stream);
    }

    /**
     * @brief restore read subgrid from disk
     * @param filename base filename for subgrid (and eventually hostgrid) corresponding to backup routine
     * @return pointer to subgrid (you take ownership).
     */
    static Subgrid* restore(const std::string& filename)
    {
      HostGrid* hostgrid = restoreHostgrid(filename);
      return restoreSubgrid(hostgrid, filename);
    }

    /**
     * @brief restore read host- and subgrid from stream
     * @param stream where host- and subgrid are consecutively given
     * @return pointer to subgrid (you take ownership of host- and subgrid!).
     */
    static Subgrid* restore(std::istream& stream)
    {
      return restoreSubgrid(stream);
    }

    /**
     * @brief restore read subgrid from disk
     * @param hostgrid the hostgrid of the subgrid
     * @param filename base filename for subgrid corresponding to backup routine
     * @return pointer to subgrid (you take ownership).
     */
    static Subgrid* restore(HostGrid* hostgrid, const std::string& filename)
    {
      return restoreSubgrid(hostgrid, filename);
    }
    /**
     * @brief restore read subgrid from stream
     * @param hostgrid the hostgrid of the subgrid
     * @param stream where subgrid is given
     * @return pointer to subgrid (you take ownership).
     */
    static Subgrid* restore(HostGrid* hostgrid, std::istream& stream)
    {
      return restoreSubgrid(hostgrid, stream);
    }

  private:
    //! \brief helper method for unified hostfilename suffix
    static std::string hostfilename(std::string filename)
    {
      std::string hostfilename = filename;
      hostfilename.append(".hostgrid");
      return hostfilename;
    }

    //! \brief helper method for unified subfilename suffix
    static std::string subfilename(std::string filename)
    {
      std::string subfilename = filename;
      subfilename.append(".subgridelements");
      return subfilename;
    }

    //! \brief hostgrid backup forwarding
    static void backupHostgrid(const HostGrid& hostgrid, const std::string& filename)
    {
      Dune::BackupRestoreFacility<HostGrid>::backup(hostgrid, hostfilename(filename));
    }

    //! \brief hostgrid backup forwarding
    static void backupHostgrid(const HostGrid& hostgrid, std::ostream& stream)
    {
      Dune::BackupRestoreFacility<HostGrid>::backup(hostgrid, stream);
    }

    //! \brief writes subgrid to filename
    static void backupSubgrid(const Subgrid& subgrid, const std::string& filename)
    {
      std::ofstream subgridStream(subfilename(filename).c_str(), std::ios::trunc | std::ios::out);
      backupSubgrid(subgrid, subgridStream);
    }

    //! \brief writes subgrid to stream
    static void backupSubgrid(const Subgrid& subgrid, std::ostream& stream)
    {
      for (auto&& e : elements(subgrid.leafGridView()))
        stream << subgrid.globalIdSet().template id<0>(e) << " ";
    }

    //! \brief hostgrid restore forwarding
    static HostGrid* restoreHostgrid(const std::string& filename)
    {
      HostGrid* hostgrid = Dune::BackupRestoreFacility<HostGrid>::restore(hostfilename(filename));
      assert(hostgrid && "Failed to restore HostGrid from filename.");
      return hostgrid;
    }

    //! \brief hostgrid restore forwarding
    static HostGrid* restoreHostGrid(std::istream& stream)
    {
      HostGrid* hostgrid = Dune::BackupRestoreFacility<HostGrid>::restore(stream);
      assert(hostgrid && "Failed to restore HostGrid from stream.");
      return hostgrid;
    }

    //! \brief restores subgrid from filename
    static Subgrid* restoreSubgrid(HostGrid* hostgrid, const std::string& filename)
    {
      std::ifstream subgridStream(subfilename(filename).c_str());
      return restoreSubgrid(hostgrid, subgridStream);
    }

    //! \brief restores subgrid from stream
    static Subgrid* restoreSubgrid(HostGrid* hostgrid, std::istream& stream)
    {
      Subgrid* subgrid = new Subgrid(*hostgrid);

      std::set<int> subgridElts;
      while (not stream.eof())
      {
        int ID;
        stream >> ID;
        subgridElts.insert(ID);
      }

      subgrid->createBegin();
      subgrid->insertSetPartial(subgridElts);
      subgrid->createEnd();
      std::cout << "SubGrid created from " << subgridElts.size() << " LeafElements." << std::endl;

      return subgrid;
    }
};

} // namespace Dune

#endif
