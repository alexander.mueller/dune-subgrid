#ifndef DUNE_SUBGRIDGEOMETRY_HH
#define DUNE_SUBGRIDGEOMETRY_HH

/** \file
* \brief The SubGridGeometry class and its specializations
*/
#include <type_traits>

#include <dune/common/version.hh>
#if DUNE_VERSION_LT(DUNE_COMMON,2,8)
#include <dune/common/std/variant.hh>
#else
#include <variant>
#endif

#include <dune/common/fmatrix.hh>
#include <dune/common/typetraits.hh>
#include <dune/geometry/multilineargeometry.hh>

namespace Dune {

template<int mydim, int coorddim, class GridImp> class SubGridGeometry;
template<int mydim, int coorddim, class GridImp> class SubGridLocalGeometry;

/** \brief Wrapping class for host grid Geometries and LocalGeometries

    \tparam isLocal If true we are wrapping a LocalGeometry

    I cannot put the isLocal parameter directly in the SubGridGeometry class,
    because the Dune grid interface specifies the number and types of template
    parameters of that class.
*/
template<int mydim, int coorddim, class GridImp>
class SubGridGeometry
{
        using ctype = typename GridImp::ctype;

    public:

        // The codimension of this entitypointer wrt the host grid
        enum {CodimInHostGrid = GridImp::HostGridType::dimension - mydim};
        enum {DimensionWorld = GridImp::HostGridType::dimensionworld};

        using HostGridGeometry = typename GridImp::HostGridType::Traits::template Codim<CodimInHostGrid>::Geometry;

        //! The type of the transposed Jacobian matrix of the mapping from the reference element to this element
        typedef typename HostGridGeometry::JacobianTransposed JacobianTransposed;
        //! The type of the inverse of the transposed Jacobian matrix of the mapping from the reference element to this element
        typedef typename HostGridGeometry::JacobianInverseTransposed JacobianInverseTransposed;
        //! The type of the Jacobian matrix of the mapping from the reference element to this element
        typedef typename HostGridGeometry::Jacobian Jacobian;
        //! The type of the inverse of the Jacobian matrix of the mapping from the reference element to this element
        typedef typename HostGridGeometry::JacobianInverse JacobianInverse;

        /** Constructor for a given host grid geometry
        */
        SubGridGeometry(const HostGridGeometry& hostGeometry)
            : hostGeometry_(hostGeometry)
        {
        }


        /** \brief Return the element type identifier
        */
        GeometryType type () const {
            return hostGeometry_.type();
        }


        /** \brief Return true if the geometry mapping is affine and false otherwise */
        bool affine() const {
            return hostGeometry_.affine();
        }

        //! return the number of corners of this element. Corners are numbered 0...n-1
        int corners () const {
            return hostGeometry_.corners();
        }


        //! access to coordinates of corners. Index is the number of the corner
        FieldVector<ctype, coorddim> corner (int i) const {
            return hostGeometry_.corner(i);
        }


        /** \brief Maps a local coordinate within reference element to
        * global coordinate in element  */
        FieldVector<ctype, coorddim> global (const FieldVector<ctype, mydim>& local) const{
            return hostGeometry_.global(local);
        }


        /** \brief Maps a global coordinate within the element to a
        * local coordinate in its reference element */
        FieldVector<ctype, mydim> local (const FieldVector<ctype, coorddim>& global) const {
            return hostGeometry_.local(global);
        }


        /**
        */
        ctype integrationElement (const FieldVector<ctype, mydim>& local) const {
            return hostGeometry_.integrationElement(local);
        }

        /** \brief return volume of geometry */
        ctype volume () const
        {
            return hostGeometry_.volume();
        }

        /** \brief return center of geometry
        *  Note that this method is still subject to a change of name and semantics.
        *  At the moment, the center is not required to be the centroid of the
        *  geometry, or even the centroid of its corners. This makes the current
        *  default implementation acceptable, which maps the centroid of the
        *  reference element to the geometry.
        *  We may change the name (and semantic) of the method to centroid() if we
        *  find reasonably efficient ways to implement it properly.
        */
        FieldVector<ctype, coorddim> center () const
        {
            return hostGeometry_.center();
        }

        //! The transposed Jacobian matrix of the mapping from the reference element to this element
        const JacobianTransposed jacobianTransposed (const FieldVector<ctype, mydim>& local) const {
            return hostGeometry_.jacobianTransposed(local);
        }

        //! The inverse of the transposed Jacobian matrix of the mapping from the reference element to this element
        const JacobianInverseTransposed jacobianInverseTransposed (const FieldVector<ctype, mydim>& local) const {
            return hostGeometry_.jacobianInverseTransposed(local);
        }

        //! The Jacobian matrix of the mapping from the reference element to this element
        const Jacobian jacobian (const FieldVector<ctype, mydim>& local) const {
            return hostGeometry_.jacobian(local);
        }

        //! The inverse of the Jacobian matrix of the mapping from the reference element to this element
        const JacobianInverse jacobianInverse (const FieldVector<ctype, mydim>& local) const {
            return hostGeometry_.jacobianInverse(local);
        }

    private:
        const HostGridGeometry hostGeometry_;
};

/** \brief Local SubGrid geometries. They can be either a wrapped hostgrid geometry or a multilineargeometry. */
template<int mydim, int coorddim, class GridImp>
class SubGridLocalGeometry
{

        using ctype = typename GridImp::ctype;

    public:
        constexpr static int hostDim = GridImp::HostGridType::dimension;
        constexpr static int dim = hostDim;
        constexpr static int CodimInHostGrid = hostDim - mydim;

    private:
        // the local geometry is either a wrapped hostgrid local geometry or a multilineargeometry
        // TODO does this work for arbitratry co-dimensions?
        using HostGridLocalGeometry = typename GridImp::HostGridType::Traits::template Codim<CodimInHostGrid>::LocalGeometry;
        using MultiLinGeometry = MultiLinearGeometry<ctype, mydim, coorddim>;

        // use a variant to store either a HostGridLocalGeometry or a MultiLinearGeometry
#if DUNE_VERSION_LT(DUNE_COMMON,2,8)
        using GeometryContainer = Std::variant<HostGridLocalGeometry, MultiLinGeometry>;
#else
        using GeometryContainer = std::variant<HostGridLocalGeometry, MultiLinGeometry>;
#endif

    public:

        // The codimension of this entitypointer wrt the host grid
        //enum {CodimInHostGrid = GridImp::HostGridType::dimension - mydim};
        enum {DimensionWorld = GridImp::HostGridType::dimensionworld};


        //! The type of the transposed Jacobian matrix of the mapping from the reference element to this element
        using JacobianTransposed = typename std::common_type<FieldMatrix< ctype, mydim, coorddim>,
                                   typename HostGridLocalGeometry::JacobianTransposed>::type;

        //! The type of the inverse of the transposed Jacobian matrix of the mapping from the reference element to this element
        using JacobianInverseTransposed = typename std::common_type<Dune::FieldMatrix<ctype,coorddim, mydim>,
                                          typename HostGridLocalGeometry::JacobianInverseTransposed>::type;

        //! The type of the Jacobian matrix of the mapping from the reference element to this element
        using Jacobian = typename std::common_type<FieldMatrix< ctype, coorddim, mydim>,
                         typename HostGridLocalGeometry::Jacobian>::type;

        //! The type of the inverse of the Jacobian matrix of the mapping from the reference element to this element
        using JacobianInverse = typename std::common_type<Dune::FieldMatrix<ctype,mydim,coorddim>,
                                typename HostGridLocalGeometry::JacobianInverse>::type;

        /** \brief Construct from a given host grid geometry*/
        SubGridLocalGeometry(HostGridLocalGeometry hostLocalGeometry)
            : localGeometry_(std::move(hostLocalGeometry))
        {}

        /** \brief Construct from MultiLinearGeometry */
        SubGridLocalGeometry(MultiLinGeometry multiLinGeometry)
            : localGeometry_(std::move(multiLinGeometry))
        {}

        /** \brief Return the element type identifier
        */
        GeometryType type () const {
#if DUNE_VERSION_LT(DUNE_COMMON,2,8)
            return Std::visit([&](auto&& geom) {return geom.type();}, localGeometry_);
#else
            return std::visit([&](auto&& geom) {return geom.type();}, localGeometry_);
#endif
        }

        /** \brief Return true if the geometry mapping is affine and false otherwise */
        bool affine() const {
#if DUNE_VERSION_LT(DUNE_COMMON,2,8)
            return Std::visit([&](auto&& geom) {return geom.affine();}, localGeometry_);
#else
            return std::visit([&](auto&& geom) {return geom.affine();}, localGeometry_);
#endif
        }

        //! return the number of corners of this element. Corners are numbered 0...n-1
        int corners () const {
#if DUNE_VERSION_LT(DUNE_COMMON,2,8)
            return Std::visit([&](auto&& geom) {return geom.corners();}, localGeometry_);
#else
            return std::visit([&](auto&& geom) {return geom.corners();}, localGeometry_);
#endif
        }

        //! access to coordinates of corners. Index is the number of the corner
        FieldVector<ctype, coorddim> corner (int i) const {
#if DUNE_VERSION_LT(DUNE_COMMON,2,8)
            return Std::visit([&](auto&& geom) {return geom.corner(i);}, localGeometry_);
#else
            return std::visit([&](auto&& geom) {return geom.corner(i);}, localGeometry_);
#endif
        }

        /** \brief Maps a local coordinate within reference element to
        * global coordinate in element  */
        FieldVector<ctype, coorddim> global (const FieldVector<ctype, mydim>& local) const {
#if DUNE_VERSION_LT(DUNE_COMMON,2,8)
            return Std::visit([&](auto&& geom) {return geom.global(local);}, localGeometry_);
#else
            return std::visit([&](auto&& geom) {return geom.global(local);}, localGeometry_);
#endif
        }

        /** \brief Maps a global coordinate within the element to a
        * local coordinate in its reference element */
        FieldVector<ctype, mydim> local (const FieldVector<ctype, coorddim>& global) const {
#if DUNE_VERSION_LT(DUNE_COMMON,2,8)
            return Std::visit([&](auto&& geom) {return geom.local(global);}, localGeometry_);
#else
            return std::visit([&](auto&& geom) {return geom.local(global);}, localGeometry_);
#endif
        }

        /**
        */
        ctype integrationElement (const FieldVector<ctype, mydim>& local) const {
#if DUNE_VERSION_LT(DUNE_COMMON,2,8)
            return Std::visit([&](auto&& geom) {return geom.integrationElement(local);}, localGeometry_);
#else
            return std::visit([&](auto&& geom) {return geom.integrationElement(local);}, localGeometry_);
#endif
        }

        /** \brief return volume of geometry */
        ctype volume () const {
#if DUNE_VERSION_LT(DUNE_COMMON,2,8)
            return Std::visit([&](auto&& geom) {return geom.volume();}, localGeometry_);
#else
            return std::visit([&](auto&& geom) {return geom.volume();}, localGeometry_);
#endif
        }

        /** \brief return center of geometry
        *  Note that this method is still subject to a change of name and semantics.
        *  At the moment, the center is not required to be the centroid of the
        *  geometry, or even the centroid of its corners. This makes the current
        *  default implementation acceptable, which maps the centroid of the
        *  reference element to the geometry.
        *  We may change the name (and semantic) of the method to centroid() if we
        *  find reasonably efficient ways to implement it properly.
        */
        FieldVector<ctype, coorddim> center () const {
#if DUNE_VERSION_LT(DUNE_COMMON,2,8)
            return Std::visit([&](auto&& geom) {return geom.center();}, localGeometry_);
#else
            return std::visit([&](auto&& geom) {return geom.center();}, localGeometry_);
#endif
        }

        //! The transposed Jacobian matrix of the mapping from the reference element to this element
        const JacobianTransposed jacobianTransposed (const FieldVector<ctype, mydim>& local) const {
#if DUNE_VERSION_LT(DUNE_COMMON,2,8)
            return Std::visit([&](auto&& geom) {return static_cast<JacobianTransposed>(geom.jacobianTransposed(local));}, localGeometry_);
#else
            return std::visit([&](auto&& geom) {return static_cast<JacobianTransposed>(geom.jacobianTransposed(local));}, localGeometry_);
#endif
        }

        //! The inverse of the transposed Jacobian matrix of the mapping from the reference element to this element
        const JacobianInverseTransposed jacobianInverseTransposed (const FieldVector<ctype, mydim>& local) const {
#if DUNE_VERSION_LT(DUNE_COMMON,2,8)
            return Std::visit([&](auto&& geom) {return static_cast<JacobianInverseTransposed>(geom.jacobianInverseTransposed(local));}, localGeometry_);
#else
            return std::visit([&](auto&& geom) {return static_cast<JacobianInverseTransposed>(geom.jacobianInverseTransposed(local));}, localGeometry_);
#endif
        }

        //! The Jacobian matrix of the mapping from the reference element to this element
        const Jacobian jacobian (const FieldVector<ctype, mydim>& local) const {
#if DUNE_VERSION_LT(DUNE_COMMON,2,8)
            return Std::visit([&](auto&& geom) {return static_cast<Jacobian>(geom.jacobian(local));}, localGeometry_);
#else
            return std::visit([&](auto&& geom) {return static_cast<Jacobian>(geom.jacobian(local));}, localGeometry_);
#endif
        }

        //! The inverse of the Jacobian matrix of the mapping from the reference element to this element
        const JacobianInverse jacobianInverse (const FieldVector<ctype, mydim>& local) const {
#if DUNE_VERSION_LT(DUNE_COMMON,2,8)
            return Std::visit([&](auto&& geom) {return static_cast<JacobianInverse>(geom.jacobianInverse(local));}, localGeometry_);
#else
            return std::visit([&](auto&& geom) {return static_cast<JacobianInverse>(geom.jacobianInverse(local));}, localGeometry_);
#endif
        }

    private:
        const GeometryContainer localGeometry_;
};

}  // namespace Dune

#endif
