#ifndef DUNE_SUBGRID_SUBGRID_LEVEL_INTERSECTION_ITERATOR_HH
#define DUNE_SUBGRID_SUBGRID_LEVEL_INTERSECTION_ITERATOR_HH

#include <list>
#include <memory>
#include <dune/common/shared_ptr.hh>

/** \file
 * \brief The SubGridLevelIntersectionIterator class
 */

namespace Dune {

// forward declaration
template <class GridImp> class SubGridLevelIntersection;




//! The iterator for intersections between elements of the same level.
template<class GridImp>
class SubGridLevelIntersectionIterator
{

    enum {dim=GridImp::dimension};

    enum {dimworld=GridImp::dimensionworld};

    // The type used to store coordinates
    typedef typename GridImp::ctype ctype;

    typedef typename GridImp::HostGridType::LevelIntersectionIterator HostLevelIntersectionIterator;
    typedef SubGridLevelIntersection<GridImp> LevelIntersectionImpl;

public:

    typedef typename GridImp::LevelIntersection Intersection;

    SubGridLevelIntersectionIterator()
    {}

    SubGridLevelIntersectionIterator(const GridImp* subGrid,
                HostLevelIntersectionIterator hostIterator)
        : subGrid_(subGrid),
        hostIterator_(std::move(hostIterator))
    {}

    //! Copy constructor
    SubGridLevelIntersectionIterator(const SubGridLevelIntersectionIterator& other) :
        subGrid_(other.subGrid_),
        hostIterator_(other.hostIterator_)
    {}

    //! Move constructor
    SubGridLevelIntersectionIterator(SubGridLevelIntersectionIterator&& other) :
        subGrid_(other.subGrid_),
        hostIterator_(std::move(other.hostIterator_))
    {}

    //! Assign operator
    SubGridLevelIntersectionIterator& operator=(const SubGridLevelIntersectionIterator& other)
    {
        subGrid_ = other.subGrid_;
        hostIterator_ = other.hostIterator_;
        return *this;
    }

    //! Assign move operator
    SubGridLevelIntersectionIterator& operator=(SubGridLevelIntersectionIterator&& other)
    {
        subGrid_ = other.subGrid_;
        hostIterator_ = std::move(other.hostIterator_);
        return *this;
    }


    //! equality
    bool equals(const SubGridLevelIntersectionIterator<GridImp>& other) const {
        return hostIterator_ == other.hostIterator_;
    }

    //! prefix increment
    void increment() {
        ++hostIterator_;
    }

    //! \brief dereferencing
    Intersection dereference() const {
        return LevelIntersectionImpl(subGrid_,*hostIterator_);
    }

private:
    const GridImp* subGrid_;
    HostLevelIntersectionIterator hostIterator_;

};



}  // namespace Dune

#endif // DUNE_SUBGRID_SUBGRID_LEVEL_INTERSECTION_ITERATOR_HH
