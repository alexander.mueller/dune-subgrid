#ifndef DUNE_SUBGRIDHIERITERATOR_HH
#define DUNE_SUBGRIDHIERITERATOR_HH

/** \file
* \brief The SubGridHierarchicIterator class
*/

namespace Dune {


//**********************************************************************
//
/**
 * \brief Iterator over the descendants of an entity.
 * \ingroup SubGrid
 * Mesh entities of codimension 0 ("elements") allow to visit all entities of
 * codimension 0 obtained through nested, hierarchic refinement of the entity.
 * Iteration over this set of entities is provided by the HierarchicIterator,
 * starting from a given entity.
 */
template<class GridImp>
class SubGridHierarchicIterator :
    public Dune::SubGridEntityIterator<0, GridImp, typename GridImp::HostGridType::HierarchicIterator>
{
    private:

        typedef typename GridImp::HostGridType::HierarchicIterator HostHierarchicIterator;
        typedef SubGridEntityIterator<0, GridImp, HostHierarchicIterator> Base;


        typedef SubGridEntity <0, GridImp::dimension, GridImp> SubGridElement;

        using Base::hostIterator_;
        using Base::subGridContainsHostIterator;

    public:


        //! Constructor
        explicit SubGridHierarchicIterator(const GridImp* subGrid, const SubGridElement& startEntity, int maxLevel) :
            Base(subGrid, startEntity.hostEntity().hbegin(maxLevel)),
            hostHierarchicEndIterator_(startEntity.hostEntity().hend(maxLevel))
        {
            // Do nothing if there are no children in the hostgrid
            if (hostIterator_ == hostHierarchicEndIterator_)
                return;

            // Search for the first entity which is in the subgrid
            if (not subGridContainsHostIterator())
                increment();
            return;
        }


        /**
         * \brief Constructor which create the end iterator
         * 
         * \param endDummy Here only to distinguish it from the other constructor
         */
        explicit SubGridHierarchicIterator(const GridImp* subGrid, const SubGridElement& startEntity, int maxLevel, [[maybe_unused]] bool endDummy) :
            Base(subGrid, startEntity.hostEntity().hend(maxLevel)),
            hostHierarchicEndIterator_(hostIterator_)
        {}


        //! prefix increment
        void increment()
        {
            do
            {
                ++hostIterator_;
                if (hostIterator_ == hostHierarchicEndIterator_)
                    return;

            }
            while (not subGridContainsHostIterator());
        }


    private:

        /**
         * \brief Cached hostgrid end iterator
         *
         * We store a copy of the hostgrid end iterator since we need to access it several times
         */
        HostHierarchicIterator hostHierarchicEndIterator_;

};


}  // end namespace Dune

#endif
