#ifndef DUNE_SUBGRIDLEAFITERATOR_HH
#define DUNE_SUBGRIDLEAFITERATOR_HH

#include <dune/grid/common/gridenums.hh>
#include <dune/subgrid/subgrid/subgridentityiterator.hh>

/** \file
* \brief The SubGridLeafIterator class
*/

namespace Dune {




/** \brief Iterator over all leaf entities of a given codimension of a grid.
*  \ingroup SubGrid
*/
template<int codim, PartitionIteratorType pitype, class GridImp>
class SubGridLeafIterator :
    public Dune::SubGridEntityIterator<codim, GridImp, typename GridImp::HostGridType::Traits::template Codim<codim>::LevelIterator>
{
    private:

        typedef typename GridImp::HostGridType::Traits::template Codim<codim>::LevelIterator HostLevelIterator;
        typedef SubGridEntityIterator<codim, GridImp, HostLevelIterator> Base;

        using Base::subGrid_;
        using Base::hostIterator_;

    public:

        SubGridLeafIterator()
        {}

        //! Move Constructor
        SubGridLeafIterator(SubGridLeafIterator&& other) :
            Base(other.subGrid_, std::move(other.hostIterator_)),
            hostLevelEndIterator_(std::move(other.hostLevelEndIterator_)),
            level_(other.level_)
        {}

        //! Copy Constructor
        SubGridLeafIterator(const SubGridLeafIterator& other) :
            Base(other.subGrid_, other.hostIterator_),
            hostLevelEndIterator_(other.hostLevelEndIterator_),
            level_(other.level_)
        {}

        //! Constructor
        explicit SubGridLeafIterator(const GridImp* subGrid) :
            Base(subGrid, subGrid->getHostGrid().levelGridView(0).template begin<codim>()),
            hostLevelEndIterator_(subGrid->getHostGrid().levelGridView(0).template end<codim>()),
            level_(0)
        {
            // Search for the first entity which is in the subgrid
            if (not (subGrid_->template isLeaf<codim>(*hostIterator_)))
                increment();
        }


        /**
         * \brief Constructor which create the end iterator
         * 
         * \param endDummy Here only to distinguish it from the other constructor
         */
        explicit SubGridLeafIterator(const GridImp* subGrid, [[maybe_unused]] bool endDummy) :
            Base(subGrid, subGrid->getHostGrid().levelGridView(subGrid->maxLevel()).template end<codim>()),
            hostLevelEndIterator_(subGrid->getHostGrid().levelGridView(0).template end<codim>()),
            level_(0)
        {}

     //! Assignement operator
        SubGridLeafIterator& operator=(const SubGridLeafIterator& other)
        {
            subGrid_ = other.subGrid_;
            hostIterator_ = other.hostIterator_;
            hostLevelEndIterator_ = other.hostLevelEndIterator_;
            level_ = other.level_;
            return *this;
        }

        //! Move assignement operator
        SubGridLeafIterator& operator=(SubGridLeafIterator&& other)
        {
            subGrid_ = other.subGrid_;
            hostIterator_ = std::move(other.hostIterator_);
            hostLevelEndIterator_ = std::move(other.hostLevelEndIterator_);
            level_ = other.level_;
            return *this;
        }

        //! prefix increment
        void increment() {
            do
            {
                ++hostIterator_;
                if (hostIterator_ == hostLevelEndIterator_)
                {
                    if (level_ < subGrid_->maxLevel())
                    {
                        ++level_;
                        auto view = subGrid_->getHostGrid().levelGridView(level_);
                        hostIterator_ = view.template begin<codim>();
                        hostLevelEndIterator_ = view.template end<codim>();
                    }
                    else
                        return;
                }
            }
            while (not (subGrid_->template isLeaf<codim>(*hostIterator_)));
        }


    private:

        /**
         * \brief Cached hostgrid end iterator
         *
         * We store a copy of the hostgrid end iterator since we need to access it several times
         */
        HostLevelIterator hostLevelEndIterator_;

        int level_;

};


}  // end namespace Dune

#endif
