#ifndef DUNE_SUBGRID_ENTITY_HH
#define DUNE_SUBGRID_ENTITY_HH

/**
 * \file
 * \brief The SubGridEntity class
 */


namespace Dune {


// Forward declarations

template<int codim, int dim, class GridImp>
class SubGridEntity;

template<class GridImp>
class SubGridLevelIntersectionIterator;

template<class GridImp>
class SubGridLeafIntersectionIterator;

template<class GridImp>
class SubGridHierarchicIterator;



/**
 * \brief The base class for implementation of entities in a SubGrid
 * \ingroup SubGrid
 *
 * This is the common base class for SubGridEntity<codim, dim, GridImp>
 * with codim==0 and codim>0.
 */
template<int codim, int dim, class GridImp>
class SubGridEntityBase :
    public EntityDefaultImplementation <codim,dim,GridImp,SubGridEntity>
{
    protected:

        typedef typename GridImp::ctype ctype;

        // The world dimension
        enum {dimworld = GridImp::dimensionworld};

        // The equivalent entity in the host grid
        typedef typename GridImp::HostGridType::Traits::template Codim<codim>::Entity HostEntity;

        // The type of the actual geometry implementation of this entity
        typedef SubGridGeometry<dim - codim, dimworld, GridImp> GeometryImpl;

        //! The type of the Geometry interface class
        typedef typename GridImp::template Codim<codim>::Geometry Geometry;

        //! The type of the EntitySeed interface class
        typedef typename GridImp::template Codim<codim>::EntitySeed EntitySeed;

        //! Default Constructor
        SubGridEntityBase()
        {}

        /** \brief Construct entity from subgrid and hostentity
         *
         * Only to be used from derived class to ensure that the static_cast to the
         * derived class SubGridEntity in EntityDefaultImplementation is OK.
         */
        SubGridEntityBase(const GridImp* subGrid, HostEntity hostEntity) :
            subGrid_(subGrid),
            hostEntity_(std::move(hostEntity))
        {}


    public:

        //! Create EntitySeed
        EntitySeed seed () const
        {
            return EntitySeed(hostEntity_);
        }

        //! Level of this entity
        int level () const {
            return hostEntity_.level();
        }

        //! The partition type for parallel computing
        PartitionType partitionType () const {
            return hostEntity_.partitionType();
        }

        //! Return copy of geometry of this entity
        Geometry geometry () const
        {
            return Geometry(GeometryImpl(hostEntity_.geometry()));
        }

        //! GeometryType of this entity
        GeometryType type () const
        {
            return hostEntity_.type();
        }

        //! Get associated hostgrid entity
        const HostEntity& hostEntity() const
        {
            return hostEntity_;
        }

        //! Grid containing this entity
        const GridImp& grid() const
        {
            return *subGrid_;
        }

        bool equals(const SubGridEntityBase& other) const
        {
            return hostEntity_ == other.hostEntity_;
        }

    protected:

        const GridImp* subGrid_;
        HostEntity hostEntity_;

};



/**
 * \brief The implementation of entities in a SubGrid
 *   \ingroup SubGrid
 *
 *  A Grid is a container of grid entities. An entity is parametrized by the codimension.
 *  An entity of codimension c in dimension d is a d-c dimensional object.
 *
 */
template<int codim, int dim, class GridImp>
class SubGridEntity :
    public SubGridEntityBase<codim, dim, GridImp>
{
    private:

        //! The base class implementing the interface for all codimensions
        typedef SubGridEntityBase<codim, dim, GridImp> Base;

        //! Import types from the base class
        typedef typename Base::HostEntity HostEntity;

    public:

        //! Default Constructor
        SubGridEntity()
        {}

        //! Construct entity from subgrid and hostentity
        SubGridEntity(const GridImp* subGrid, HostEntity hostEntity) :
            Base(subGrid, std::move(hostEntity))
        {}

        //! Copy constructor
        SubGridEntity(const SubGridEntity& other) :
            Base(other.subGrid_,other.hostEntity_)
        {}

        //! Move constructor
        SubGridEntity(SubGridEntity&& other) :
            Base(other.subGrid_,std::move(other.hostEntity_))
        {}

        /** \brief Move assignment operator. */
        SubGridEntity& operator=(const SubGridEntity& other) {
            this->subGrid_ = other.subGrid_;
            this->hostEntity_ = other.hostEntity_;
            return *this;
        }

        /** \brief Move assignment operator. */
        SubGridEntity& operator=(SubGridEntity&& other) {
            this->subGrid_ = other.subGrid_;
            this->hostEntity_ = std::move(other.hostEntity_);
            return *this;
        }
};




/**
 * \brief Specialization for codim-0-entities.
 * \ingroup SubGrid
 *
 * This class embodies the topological parts of elements of the grid.
 * It has an extended interface compared to the general entity class.
 * For example, Entities of codimension 0  allow to visit all neighbors.
 */
template<int dim, class GridImp>
class SubGridEntity<0, dim, GridImp> :
    public SubGridEntityBase<0, dim, GridImp>
{
    private:

        //! The base class implementing the interface for all codimensions
        typedef SubGridEntityBase<0, dim, GridImp> Base;

        //! Import types from the base class
        typedef typename Base::HostEntity HostEntity;

        //! Import members of base class
        using Base::dimworld;
        using Base::subGrid_;
        using Base::hostEntity_;

        //! The type of the actual local geometry implementation of the geometryInFather map
        typedef SubGridLocalGeometry<dim, dimworld, GridImp> LocalGeometryImpl;

        typedef typename GridImp::template Codim<0>::Entity Entity;
    public:

        //! The type of the LocalGeometry interface class
        typedef typename GridImp::template Codim<0>::LocalGeometry LocalGeometry;

        //! Default Constructor
        SubGridEntity()
        {}

        //! Copy Constructor
        SubGridEntity(const SubGridEntity& other) :
            Base(other.subGrid_, other.hostEntity_)
        {}

        //! Move Constructor
        SubGridEntity(SubGridEntity&& other) :
            Base(other.subGrid_, std::move(other.hostEntity_))
        {}

        //! Construct entity from subgrid and hostentity
        SubGridEntity(const GridImp* subGrid, HostEntity hostEntity) :
            Base(subGrid, std::move(hostEntity))
        {}

        //! Assignment operator. */
        SubGridEntity& operator=(const SubGridEntity& other) {
            this->subGrid_ = other.subGrid_;
            this->hostEntity_ = other.hostEntity_;
            return *this;
        }


        //! Move assignment operator. */
        SubGridEntity& operator=(SubGridEntity&& other) {
            this->subGrid_ = other.subGrid_;
            this->hostEntity_ = std::move(other.hostEntity_);
            return *this;
        }

        /**
         * \brief Provide access to sub entity i of given codimension.
         * Entities are numbered 0 ... count<cc>()-1
         */
        template<int cc>
        typename GridImp::template Codim<cc>::Entity subEntity (int i) const
        {
            return SubGridEntity<cc,dim,GridImp>(subGrid_, hostEntity_.template subEntity<cc>(i));
        }

        /**
         * \brief Number of subentities with codimension codim
         */
        unsigned int subEntities (unsigned int codim) const
        {
            return hostEntity_.subEntities(codim);
        }

        //! First level intersection
        SubGridLevelIntersectionIterator<GridImp> ilevelbegin () const
        {
            return SubGridLevelIntersectionIterator<GridImp>(
                    subGrid_,
                    subGrid_->hostIIFactory().iLevelBegin(hostEntity_));
        }


        //! Reference to one past the last neighbor
        SubGridLevelIntersectionIterator<GridImp> ilevelend () const
        {
            return SubGridLevelIntersectionIterator<GridImp>(
                    subGrid_,
                    subGrid_->hostIIFactory().iLevelEnd(hostEntity_));
        }


        //! First leaf intersection
        SubGridLeafIntersectionIterator<GridImp> ileafbegin () const
        {
            if (!isLeaf()) return ileafend();
            return SubGridLeafIntersectionIterator<GridImp>(
                    subGrid_,
                    subGrid_->hostIIFactory().iLevelBegin(hostEntity_),
                    subGrid_->hostIIFactory().iLevelEnd(hostEntity_));
        }


        //! Reference to one past the last leaf intersection
        SubGridLeafIntersectionIterator<GridImp> ileafend () const
        {
            return SubGridLeafIntersectionIterator<GridImp>(
                    subGrid_,
                    subGrid_->hostIIFactory().iLevelEnd(hostEntity_),
                    subGrid_->hostIIFactory().iLevelEnd(hostEntity_));
        }


        //! returns true if Entity has NO children
        bool isLeaf() const {
            return subGrid_->template isLeaf<0>(hostEntity_);
        }


        /**
         * \brief Inter-level access to father element on coarser grid.
         *
         * Assumes that meshes are nested.
         */
        Entity father () const {
            return SubGridEntity<0,dim,GridImp>(subGrid_, hostEntity_.father());
        }


        /**
         * \brief Return true if entity has a father entity which can be accessed
         * using the father() method.
         */
        bool hasFather () const
        {
            return hostEntity_.hasFather();
        }


        /**
         * \brief Location of this element relative to the reference element element of the father.
         *
         * This is sufficient to interpolate all dofs in conforming case.
         * Nonconforming may require access to neighbors of father and
         * computations with local coordinates.
         * On the fly case is somewhat inefficient since dofs  are visited several times.
         * If we store interpolation matrices, this is tolerable. We assume that on-the-fly
         * implementation of numerical algorithms is only done for simple discretizations.
         * Assumes that meshes are nested.
         */
        LocalGeometry geometryInFather () const {
            return LocalGeometry(LocalGeometryImpl(hostEntity_.geometryInFather()));
        }


        /**
         * \brief Inter-level access to son elements on higher levels<=maxlevel.
         * This is provided for sparsely stored nested unstructured meshes.
         * Returns iterator to first son.
         */
        SubGridHierarchicIterator<GridImp> hbegin (int maxLevel) const
        {
            return SubGridHierarchicIterator<const GridImp>(subGrid_, *this, maxLevel);
        }


        //! Returns iterator to one past the last son
        SubGridHierarchicIterator<GridImp> hend (int maxLevel) const
        {
            return SubGridHierarchicIterator<const GridImp>(subGrid_, *this, maxLevel, true);
        }


        //! \todo Please doc me !
        bool wasRefined () const
        {
            return subGrid_->wasRefined(*this);
        }


        //! \todo Please doc me !
        bool mightBeCoarsened () const
        {
            return true;
        }

}; // end of SubGridEntity codim = 0


} // namespace Dune


#endif
