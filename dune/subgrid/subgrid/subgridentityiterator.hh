#ifndef DUNE_SUBGRID_ENTITY_ITERATOR_HH
#define DUNE_SUBGRID_ENTITY_ITERATOR_HH

#include <utility>
#include <dune/subgrid/subgrid/subgridentity.hh>

/** \file
* \brief The SubGridEntityIterator class
*/

namespace Dune {


/**
 * \brief The SubGridEntityIterator class
 */
template<int codim, class GridImp, class HostEntityIterator>
class SubGridEntityIterator
{
    protected:

        //! grid dimension
        enum { dim = GridImp::dimension };

        //! Type of entity implementation
        typedef SubGridEntity<codim, dim, GridImp> EntityImp;

        //! Type of the hostgrid entity
        typedef typename GridImp::HostGridType::Traits::template Codim<codim>::Entity HostEntity;

    public:

        /**
         * \brief Codimension of entity pointed to
         *
         * Needed by the interface classes.
         */
        enum { codimension = codim };

        /**
         * \brief Type of corresponding entity
         *
         * Needed by the interface classes.
         */
        typedef typename GridImp::template Codim<codim>::Entity Entity;

        SubGridEntityIterator()
        {}

        /**
         * \brief Constructor from subgrid and hostgrid Iterator
         */
        template<class OtherHostEntityIterator>
        SubGridEntityIterator(const GridImp* subGrid, OtherHostEntityIterator hostIt) :
            subGrid_(subGrid),
            hostIterator_(std::move(hostIt))
        {}

        /**
         * \brief Copy constructor
         *
         * Needed to set up entity_ correctly.
         */
        SubGridEntityIterator(const SubGridEntityIterator& other) :
            subGrid_(other.subGrid_),
            hostIterator_(other.hostIterator_)
        {}


        /**
         * \brief Assignement operator */
        SubGridEntityIterator& operator=(const SubGridEntityIterator& other)
        {
            subGrid_ = other.subGrid_;
            hostIterator_ = other.hostIterator_;

            return *this;
        }

        /**
         * \brief Assignement operator */
        SubGridEntityIterator& operator=(SubGridEntityIterator&& other)
        {
            subGrid_ = other.subGrid_;
            hostIterator_ = std::move(other.hostIterator_);

            return *this;
        }



        //! equality
        template<class OtherHostEntityIterator>
        bool equals(const SubGridEntityIterator<codim, GridImp, OtherHostEntityIterator>& other) const
        {
            return (hostIterator_ == other.hostIterator());
        }


        //! dereferencing
        Entity dereference() const
        {
            return Entity(EntityImp(subGrid_,*hostIterator_));
        }

        /**
         * \brief Access to subgrid this entity belongs to
         */
        const GridImp& grid() const
        {
            return *subGrid_;
        }


        /**
         * \brief Access to wrapped hostgrid iterator
         */
        const HostEntityIterator& hostIterator() const
        {
            return hostIterator_;
        }

    protected:

        /**
         * \brief Test if the stored hostgrid iterator is contained in the subgrid
         *
         * This does not touch the stored subgrid entity so it can be called
         * during the in increment() method of derived iterator classes
         * where the subgrid entity and the hostgrid iterator are out
         * of sync.
         */
        bool subGridContainsHostIterator() const
        {
            return subGrid_->template contains<codim>(*hostIterator_);
        }


        //! Pointer to subgrid
        const GridImp* subGrid_;

        //! Stored hostgrid iterator
        HostEntityIterator hostIterator_;
};


} // end namespace Dune

#endif
