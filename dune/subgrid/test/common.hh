#include <config.h>

#define DISABLE_DEPRECATED_METHOD_CHECK 1

#include <memory>

#include <dune/common/exceptions.hh>

#include <dune/grid/utility/structuredgridfactory.hh>
#include <dune/grid/test/gridcheck.hh>
#include <dune/grid/test/checkintersectionit.hh>
#include <dune/grid/io/file/vtk/vtkwriter.hh>

#include <dune/subgrid/subgrid.hh>

using namespace Dune;

template<class GridType, bool MapIndexStorage>
struct EnableLevelIntersectionIteratorCheck< SubGrid<GridType::dimension,GridType, MapIndexStorage> >
{
  static const bool v = false;
};

enum EltGeometryType {SIMPLEX, CUBE, NA};

template <class GridType, EltGeometryType EGT>
struct CoarseGridConstructor
{
    static std::shared_ptr<GridType> constructCoarseGrid(size_t ncells=1)
    {
        DUNE_THROW(Dune::NotImplemented,"not implemented for this Element geometry");
    }
};

template <class GridType>
struct CoarseGridConstructor<GridType, SIMPLEX>
{
    static std::shared_ptr<GridType> constructCoarseGrid(size_t ncells=1)
    {
        const int dim=GridType::dimension;
        std::array<unsigned int, dim> elts;
        elts.fill(ncells);
        return Dune::StructuredGridFactory<GridType>::createSimplexGrid(Dune::FieldVector<double,dim>(-1.0),Dune::FieldVector<double,dim>(1.0),elts);
    }
};

template <class GridType>
struct CoarseGridConstructor<GridType, CUBE>
{
    static std::shared_ptr<GridType> constructCoarseGrid(size_t ncells=1)
    {
        const int dim=GridType::dimension;
        std::array<unsigned int, dim> elts;
        elts.fill(ncells);
        return Dune::StructuredGridFactory<GridType>::createCubeGrid(Dune::FieldVector<double,dim>(-1.0),Dune::FieldVector<double,dim>(1.0),elts);
    }
};

template<class GridType>
void checkLeafIntersections(const GridType& grid) {

  const auto& gridView = grid.leafGridView();
  for(const auto& e : elements(gridView)) {

    std::vector<typename GridType::ctype> faceLengths(e.subEntities(1), 0.);
    std::vector<bool> conforming(faceLengths.size(),false);

    // compute sum of all intersections volumes
    for(const auto& is : intersections(gridView, e)) {
        conforming[is.indexInInside()] = is.conforming();
        faceLengths[is.indexInInside()] += is.geometry().volume();
    }

    // now compare with face length
    for(size_t face = 0; face < faceLengths.size(); face++) {
      auto faceLength = e.template subEntity<1>(face).geometry().volume();

      if(!conforming[face] or std::fabs(faceLength - faceLengths[face]) <= 1e-8)
        continue;

      auto eCenter = e.geometry().center();
      auto fCenter = e.template subEntity<1>(face).geometry().center();

      std::cout << "In element with center "
                << eCenter << ": face " << face<< " with center " << fCenter
                << " has length " << faceLength << ", but it's intersections only "
                << "have a combined length of " << faceLengths[face] << '\n';

      std::cout << "On this face the following intersection have been found:\n";
      for(const auto& is : intersections(gridView, e))
        if((int) face == is.indexInInside())
          std::cout << "  Intersection with center " << is.geometry().center()
                    << " has length " << is.geometry().volume() << "\n";

      DUNE_THROW(Dune::Exception, "Intersection length don't sum up to face length.");
    }
  }
}

template<class GridType>
void refineLocally(GridType& grid, int refine=3)
{
    typedef typename GridType::LevelGridView GridView;

    for(int i=0; i<refine; ++i)
    {
        GridView gv = grid.levelGridView(grid.maxLevel());
        size_t half = gv.size(0)/2 + 1;

        for(const auto& e : elements(gv))
        {
            if (gv.indexSet().index(e) < half)
                grid.mark(1, e);
        }
        grid.preAdapt();
        grid.adapt();
        grid.postAdapt();
    }
}

template<class GridType>
void createPartialDomainSubgrid(GridType& subgrid, const double r1=0.2, const double r2=0.3)
{

    typedef typename GridType::HostGridType HostGridType;
    std::set<typename HostGridType::Traits::GlobalIdSet::IdType> elementsForSubgrid;

    HostGridType& hostgrid = subgrid.getHostGrid();

    const typename HostGridType::Traits::GlobalIdSet& globalIDset = hostgrid.globalIdSet();

    const auto& hostLeafView = hostgrid.leafGridView();
    for (const auto& element : elements(hostLeafView))
    {
        auto geom = element.geometry();
        for (size_t v=0; v<(size_t) geom.corners(); ++v)
        {
            if (fabs(geom.corner(v)[0])<=r1 or fabs(geom.corner(v)[1])<=r2)
            {
                elementsForSubgrid.insert(globalIDset.template id<0>(element));
                break;
            }
        }
    }

    subgrid.createBegin();
    subgrid.insertSetPartial(elementsForSubgrid);
    subgrid.createEnd();

    return;
}

template<class GridType>
void createRingDomainSubgrid(GridType& subgrid, const double r1=0.5, const double r2=0.9)
{
    std::set<size_t> elementsForSubgrid;

    typedef typename GridType::HostGridType HostGridType;

    HostGridType& hostgrid = subgrid.getHostGrid();

    const typename HostGridType::Traits::GlobalIdSet& globalIDset = hostgrid.globalIdSet();

    const auto& hostLeafView = hostgrid.leafGridView();
    for (const auto& element : elements(hostLeafView))
    {
        double  sum = 0.0;

        auto geom = element->geometry();
        for (size_t v=0; v<geom.corners(); ++v)
        {
            sum += geom.corner(v).two_norm();
            for (size_t w=v+1; w<geom.corners(); ++w)
            {
                auto dummy=geom.corner(w);
                dummy -= geom.corner(v);

                if (dummy.two_norm()>2.0*r2)
                {
                    elementsForSubgrid.insert(globalIDset.template id<0>(element));
                    break;
                }
            }
        }
        if (sum > geom.corners()*r1 and sum < geom.corners()*r2)
            elementsForSubgrid.insert(globalIDset.template id<0>(element));
    }

    subgrid.createBegin();
    subgrid.insertSetPartial(elementsForSubgrid);
    subgrid.createEnd();

    return;
}

// test subgrid for given dimension
template <class HostGridType, EltGeometryType EGT>
size_t checkWithHostGrid(size_t host_prerefine=2, int sub_initlevel=-1, size_t sub_localrefine=3, size_t host_level0_ncells=1)
{
    size_t ret = 0;
    const int dim=HostGridType::dimension;

    std::shared_ptr<HostGridType> hostgridptr = (CoarseGridConstructor<HostGridType,EGT>::constructCoarseGrid(host_level0_ncells));
    HostGridType& hostgrid = *hostgridptr;

//    hostgrid.setClosureType(HostGridType::NONE);

    std::cout << "Macro-Hostgrid created with " << hostgrid.size(dim) << " nodes and " << hostgrid.size(0) << " elements on level 0." << std::endl;

    for (size_t i=0; i<host_prerefine; ++i)
        hostgrid.globalRefine(1);

    std::cout << "Hostgrid uniformly refined to level " << hostgrid.maxLevel() << " with " << hostgrid.size(dim) << " nodes and " << hostgrid.size(0) << " elements." << std::endl;

    typedef SubGrid<dim,HostGridType> SubGridType;
    SubGridType subgrid(hostgrid);

    // create subgrid with insertLevel
    {
        subgrid.createBegin();
        subgrid.insertLevel(hostgrid.maxLevel()+sub_initlevel);
        subgrid.createEnd();
        subgrid.setMaxLevelDifference(1);
    }

    std::cout << "Uniformly refined SubGrid created with " << subgrid.size(dim) << " nodes and " << subgrid.size(0) << " elements with maxlevel= " << subgrid.maxLevel() << "." << std::endl;

    subgrid.report();

    try {
        std::cout << "Performing gridcheck\n";
        gridcheck(subgrid);
    }
    catch (Dune::NotImplemented& e)
    {
        std::cerr << e.what() << std::endl;
    } catch (const Dune::Exception& e) {
        std::cerr << e.what() <<std::endl;
        ret = 1;
    }

    try {
        std::cout << "Performing checkIntersectionIterator\n";
        checkIntersectionIterator(subgrid);
    }
    catch (Dune::NotImplemented& e)
    {
        std::cerr << e.what() << std::endl;
    } catch (const Dune::Exception& e) {
        std::cerr << e.what() <<std::endl;
        ret = 1;
    }
    try {
        std::cout << "Perfoming checkLeafIntersections\n";
        checkLeafIntersections(subgrid);
    } catch (Dune::NotImplemented& e)
    {
        std::cerr << e.what() << std::endl;
    } catch (const Dune::Exception& e) {
        std::cerr << e.what() <<std::endl;
        ret = 1;
    }


#if 0
    //create subgrid with insertSet

    // check subgrid
    std::cout << "Check subgrid with dim=" << dim << std::endl;

    gridcheck(subgrid);
    checkIntersectionIterator(subgrid);

#endif
    // refine subgrid
    refineLocally(subgrid,sub_localrefine);
    subgrid.report();

    std::cout << "Locally refined SubGrid created with " << subgrid.size(dim) << " nodes and " << subgrid.size(0) << " elements with maxlevel= " << subgrid.maxLevel() << "." << std::endl;

    // check locally refined subgrid
    try {
        std::cout << "Performing gridcheck\n";
        gridcheck(subgrid);
    }
    catch (Dune::NotImplemented& e)
    {
        std::cerr << e.what() << std::endl;
    } catch (const Dune::Exception& e) {
        std::cerr << e.what() <<std::endl;
        ret = 1;
    }
    try {
        std::cout << "Performing checkIntersectionIterator\n";
        checkIntersectionIterator(subgrid);
    }
    catch (Dune::NotImplemented& e)
    {
        std::cerr << e.what() << std::endl;
    } catch (const Dune::Exception& e) {
        std::cerr << e.what() <<std::endl;
        ret = 1;
    }
     try {
        std::cout << "Perfoming checkLeafIntersections\n";
        checkLeafIntersections(subgrid);
    }
    catch (Dune::NotImplemented& e)
    {
        std::cerr << e.what() << std::endl;
    } catch (const Dune::Exception& e) {
        std::cerr << e.what() <<std::endl;
        ret = 1;
    }



    createPartialDomainSubgrid(subgrid);
    Dune::VTKWriter<typename SubGridType::LeafGridView> writer(subgrid.leafGridView());
    writer.write("grid");

    std::cout << "Locally refined SubGrid with domain coverage <100\\% created with " << subgrid.size(dim) << " nodes and " << subgrid.size(0) << " elements with maxlevel= " << subgrid.maxLevel() << "." << std::endl;
    try {
        std::cout << "Performing gridcheck\n";
        gridcheck(subgrid);
    }
    catch (Dune::NotImplemented& e)
    {
        std::cerr << e << std::endl;
    } catch (const Dune::Exception& e) {
        std::cerr << e.what() <<std::endl;
        ret = 1;
    }
 /* // IntersectionIterator tests only make sense if coarse grid elements are fully covered by the subgrid
    try {
        std::cout << "Performing checkIntersectionIterator\n";
        checkIntersectionIterator(subgrid);
    }
    catch (const Dune::NotImplemented& e)
    {
        std::cerr << e.what() << std::endl;
    } catch (const Dune::Exception& e) {
        std::cerr << e.what() <<std::endl;
        ret = 1;
    }
*/
    try {
        std::cout << "Perfoming checkLeafIntersections\n";
        checkLeafIntersections(subgrid);
    }
    catch (Dune::NotImplemented& e)
    {
        std::cerr << e.what() << std::endl;
    } catch (const Dune::Exception& e) {
        std::cerr << e.what() <<std::endl;
        ret = 1;
    }

    return ret;
}
