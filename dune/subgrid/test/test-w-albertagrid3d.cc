#include <config.h>

#include <dune/common/fvector.hh>
#include <dune/common/parallel/mpihelper.hh>
#include <dune/grid/albertagrid.hh>
#include <dune/subgrid/subgrid.hh>
#include <dune/subgrid/test/common.hh>

#if !(ALBERTA_DIM == 3)
#error test-w-albertagrid3d needs to be run with alberta dimension 3
#endif

using namespace Dune;

int main(int argc, char* argv[]) try {
  Dune::MPIHelper::instance(argc, argv);

  size_t return_val = 0;

  using Grid = AlbertaGrid<3, 3>;
  return_val = std::max(return_val, checkWithHostGrid<Grid, SIMPLEX>());

  std::cout << "Test for SubGrid with AlbertaGrid<3,3> " << (return_val==0 ? "passed." : "failed.") << std::endl;
  return return_val;

} catch (const Exception& e) {
  std::cout << e.what() << std::endl;
  return 1;
}
