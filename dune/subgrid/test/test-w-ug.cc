#include <config.h>

#define DISABLE_DEPRECATED_METHOD_CHECK 1

#include <dune/common/exceptions.hh>
#include <dune/grid/uggrid.hh>
#include <dune/subgrid/test/common.hh>

int main (int argc, char *argv[]) try
{
    Dune::MPIHelper::instance(argc, argv);

    typedef Dune::UGGrid<2> GridType2D;
    typedef Dune::UGGrid<3> GridType3D;

    size_t return_val = 0;

    return_val = std::max(return_val,checkWithHostGrid<GridType2D, SIMPLEX>());
//    return_val = std::max(return_val,checkWithHostGrid<GridType2D, CUBE>());
    return_val = std::max(return_val,checkWithHostGrid<GridType3D, SIMPLEX>(2, -1, 1));
 //   return_val = std::max(return_val,checkWithHostGrid<GridType3D, CUBE>());

    std::cout << "Test for SubGrid with UGGrid " << (return_val==0 ? "passed." : "failed.") << std::endl;
    return return_val;
}

// //////////////////////////////////
//   Error handler
// /////////////////////////////////
catch (const Exception& e) {
    std::cout << e.what() << std::endl;
    return 1;
}
