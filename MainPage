/**
\mainpage Dune-Subgrid Module

\section intro_sec Introduction

<a href="">Dune-subgrid</a> is a module for the <a href="http://www.dune-project.org">DUNE environment</a>.
The dune-subgrid module allows to mark elements of another hierarchical dune grid.
The set of marked elements can then be accessed as a hierarchical dune grid in its own right.
Dune-Subgrid provides the full grid interface including adaptive mesh refinement.
Further information on the features, interface and implementation of dune-subgrid
can be found in the <a href="http://www.matheon.de/preprints/5868_graeser_sander_subgrid.pdf">dune-subgrid paper</a>.

See also the page with the \ref KnownIssues "known issues".

\section deps_sec Dependencies

Dune-subgrid depends on the DUNE core modules dune-common, dune-grid and dune-istl.

\section install_sec Installation

Unpack the downloaded archive to the directory containing your dune modules
and use the dunecontrol script of the dune-common module to configure and
build it.

\section usage_sec Usage

Use the method createBegin() followed by the various insert methods to
insert elements in a subgrid. Finalize the subgrid creation by createEnd().
Please refer the class documentation for further usage information.

\section publications_sec Publications

Please \b cite the
<a href="http://www.matheon.de/preprints/5868_graeser_sander_subgrid.pdf">dune-subgrid paper</a>
in any publication using the dune-subgrid module.

C. Gr&auml;ser and O.Sander,
The dune-subgrid Module and Some Applications,
Preprint 566, Matheon, 2009.

\section license_sec License

The dune-subgrid library and headers are licensed under version 2 of the GNU
General Public License (see below), with a special exception for
linking and compiling against dune-subgrid, the so-called "runtime exception."
The license is intended to be similiar to the GNU Lesser General
Public License, which by itself isn't suitable for a template library.

The exact wording of the exception reads as follows:

As a special exception, you may use the dune-subgrid source files as part
of a software library or application without restriction.
Specifically, if other files instantiate templates or use macros or
inline functions from one or more of the dune-subgrid source files, or you
compile one or more of the dune-subgrid source files and link them with
other files to produce an executable, this does not by itself cause
the resulting executable to be covered by the GNU General Public
License.  This exception does not however invalidate any other
reasons why the executable file might be covered by the GNU General
Public License.

This licence clones the one of the libstc++ library. For further
implications of this library please see their
<a href="http://gcc.gnu.org/onlinedocs/libstdc++/manual/bk01pt01ch01s02.html">licence page</a>.

See the file COPYING for full copying permissions.

*/
